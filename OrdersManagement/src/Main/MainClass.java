package Main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.SwingUtilities;

import controller.*;
import model.*;
import view.*;

public class MainClass {
	

		public static void main(String[] args) {
			String fileName="data.bin";
			
			try {
				ObjectInputStream is= new ObjectInputStream(new FileInputStream(fileName));
				TheModel theModel;
				try {
					theModel = (TheModel) is.readObject();
					is.close();
					SwingUtilities.invokeLater(new Runnable() {
			            @Override
			            public void run() { 
			            	TheView theView;
			    			//TheModel theModel;
			            	theView= new TheView();
			        		TheController theController = new TheController(theModel,theView);
			        		theController.init();
			            }  
			        }); 
					
					Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				        public void run() {
				        	try {
								ObjectOutputStream os= new ObjectOutputStream(new FileOutputStream(fileName));
								os.writeObject(theModel);
								os.close();
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				        }
				    }, "Shutdown-thread"));
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			TheModel theModel= new TheModel();

		}
}	

		



