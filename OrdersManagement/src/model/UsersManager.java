package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Bogdan
 * Clasa UsersManager tine o evidenta a utilizatorilor din model, atat admini, cat si clienti.
 * Acestia sunt pastrati intr-o lista.
 */
public class UsersManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private List<User> users= new ArrayList<User>();
	
	//CONSTRUCTORS
	public UsersManager(){
		
	}
	
	
	/**
	 * Metoda ne ofera userul pe baza textfield-urilor username si password sau null daca acest user
	 * nu exista in lista de useri. In acest ultim caz, se va afisa un mesaj de eroare. Metoda
	 * este utila in principal la logare.
	 * @param username username-ul cautat. Acesta este introdus intr-un text field in cadrul aplicatiei
	 * @param password parola cautata. Aceasta este introdusa intr-un text field in cadrul aplicatiei
	 * @return
	 */
	public User getUser(String username, String password){
		
		
		for(User user:users){
			if ((user.getPass().equals(password)) && (user.getUsername().equals(username))){
				return user;
			}
		}
		return null;
	}
	
	/**
	 * Aceassta metoda verifica daca exista deja un client avand un username anume. Metoda
	 * este utila in principal in cadrul adaugarii de noi clienti de catre admin
	 * , moment in care este necesara o astfel de validare.
	 * @param customer Clientul introdus de care admin, a carui cheie este cautata in lista. In cazul in care
	 * s-a gasit, adminul este semnalat.
	 * @return -1 daca nu exista niciun client avand acest nume si pozitia in cadrul listei a  clientului cu acest username
	 * in cazul in care exsta deja
	 */
	public int existsCustomer(Customer customer){
		int i=0;
		for(User user: users){
			if (  (user.getUsername()).equals(customer.getUsername())){
				System.out.println("userul nu e  unic!");
				return i;
			}
			i++;
		}
		System.out.println("Userul e unic!");
		return -1;
	}
	
	/**
	 * Metoda adauga un client, asta dupa ce testeaza mai intai daca nu exista deja un client
	 * avand username-ul setat de admin.
	 * @param customer Clientul de adaugat. Datele legate de el sunt introduse de admin.
	 * @return true sau false dupa cum a reusit sau nu adaugarea clientului. Cazul de nereusita se datoreaza
	 * existentei unui client cu username-ul dorit.
	 */
	public boolean addCustomer(Customer customer){
		if (existsCustomer(customer)==-1){
			users.add(customer);
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * Metoda este utila in cadru stergerii de clienti, realizata de catre admin. In cadrul metodei
	 * se verifica daca exista sau nu acest client care se vrea sters.
	 * @param customer Clientul de sters. Acesta este selectat de admin din tabelul de clienti.
	 * @return true sau false dupa cum a reusit sau nu stergerea clientului. Cazul de nereusita
	 * se datoreaza inexistentei clientului de sters.
	 */
	public boolean deleteCustomer(Customer customer){
		int i=existsCustomer(customer);
		if(i!=-1){
			users.remove(users.remove(users.get(i)));
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * Metoda modifica informatiile legate de un client anume, la cererea adminului.
	 * Informatiile care pot fi modificare sunt numele, adresa si telefonul. 
	 * Totodata, in cadrul metodei se verifica daca exista clientul care se vrea modificat.
	 * @param customer o instanta a clientului de modificat
	 * @return true sau false dupa cum a reusit sau nu modificarea clientului. Cazul de nereusita
	 * se datoreaza inexistentei clientului de modificat.
	 */
	public boolean modifyCustomer(Customer customer){
		int i=existsCustomer(customer);
		if(i!=-1){
			((Customer)users.get(i)).setName(customer.getName());
			((Customer)users.get(i)).setAdress(customer.getAdress());
			((Customer)users.get(i)).setTelephone(customer.getTelephone());
			return true;
		}
		return false;
	}
	
	
	//GETTERS & SETTERS
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	
	
	
	
}
