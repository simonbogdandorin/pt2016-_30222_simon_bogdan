package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.Product.Category;

/**
 * 
 * @author Bogdan
 *Clasa TheModel reprezinta situatia globala aplicatiei. Prin faptul ca implementeaza interfata
 *Serializable, ea poate fi serializata in momentul iesirii din aplicatie, existand astfel posibilitatea
 *salvarii situatiei legate de depozit,clienti ,comenzi, admini etc.
 *La deschiderea aplicatiei, se realizeaza deseriaizarea.
 *
 *
 */

public class TheModel implements Serializable {
	/**
	 * 
	 */
	
	/**
	 * 
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private UsersManager usersManager = new UsersManager();
	private OPDept opdept= new OPDept();
	
	
	
	public void initDummyVal(){
		Warehouse warehouse= new Warehouse();
		List<User> users=new ArrayList<User>();
		List<Product> orderedProds = new ArrayList<Product>();
		List<Product> orderedProds2 = new ArrayList<Product>();
		
		User admin1=new Admin("simonbogdandorin",
				"simonbogdandorin");
		User customer1=new Customer("Popescu Ion","popion","passpopion",
									"Com. Recea, str.Principala, nr. 120A","0740085954");
		User customer2=new Customer("Ionescu Alex","ionescualex","ionescualex",
				"Brasov,Str.Libertatii, nr. 23","0745632334");
		users.add(admin1);
		users.add(customer1);
		users.add(customer2);
		usersManager.setUsers(users);
		
		Product prod1= new Product("laptop",Category.IT , 2500.0, 3);
		Product prod2= new Product("masina de spalat",Category.ELECTROCASNICE , 3210.50, 2);
		Product prod3= new Product("televizor",Category.ELECTROCASNICE , 1500.0, 19);
		Product prod4= new Product("rosii",Category.ALIMENTE , 2.99, 23);
		Product prod5= new Product("castraveti",Category.ALIMENTE , 1.50, 1);
		warehouse.createProd(prod1);
		warehouse.createProd(prod2);
		warehouse.createProd(prod3);
		warehouse.createProd(prod4);
		warehouse.createProd(prod5);
		opdept.setWarehouse(warehouse);	
		
		Product orderedProd1=new Product("leptop",Category.IT , 2500.0, 1);
		Product orderedProd2= new Product("rosii",Category.ALIMENTE , 2.99, 12);
		Product orderedProd3= new Product("castraveti",Category.ALIMENTE , 2.99, 1);
		
		orderedProds.add(orderedProd1);
		orderedProds.add(orderedProd2);
		orderedProds2.add(orderedProd3);
		
		Order ord= new Order(orderedProds,(Customer)customer1);
		//Order ord2=new Order(orderedProds2,(Customer)customer2);
		//opdept.addOrder(ord);
		//opdept.addOrder(ord2);
		
		

		
	}
	
	//GETTERS & SETTERS
	public UsersManager getUsersManager() {
		return usersManager;
	}



	public void setUsersManager(UsersManager usersManager) {
		this.usersManager = usersManager;
	}


	public OPDept getOpdept() {
		return opdept;
	}



	public void setOpdept(OPDept opdept) {
		this.opdept = opdept;
	}
	
	
}
