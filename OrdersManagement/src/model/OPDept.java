package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author Bogdan
 * Clas OPDept coordoneaza operatiile legate de comenzi.
 * Ea contine in acest sens un arbore binar de cautare cu toate comenzile,
 * un numar ce reprezinta total comenzilor dar si depozitul cu care se opereaza 
 * luarea de produse in cadrul comenzii.
 *
 */
public class OPDept implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private static final long serialVersionUID = 1L;
	private Map<Long,Order> orders= new TreeMap<Long,Order>();
	private Integer ordNr;
	private Warehouse warehouse;
	
	//CONSTRUCTORS
	public OPDept(Warehouse warehouse){
		this.warehouse=warehouse;
		this.ordNr=0;
	}
	public OPDept(){

		this.ordNr=0;
	}
	
	//ADAUGA COMANDA
	/**
	 * Metoda adauga o comanda ce va fi mai apoi confirmata de admin. Adaugarea comenzii este
	 * realizata, evident, de client. Metoda contine testul foarte important 
	 * de verificare UNDERSTOCK, deci va esua atunci cand comanda cere niste resurse indisponibile
	 * in depozit. Totodata, verifica daca comanda nu exista deja.
	 * @param order Comanda de adaugat
	 * @return true sau false dupa cum s-a adaugat sau nu comanda.
	 */
	public boolean addOrder(Order order){
		//VERIFIC
		if(orders.containsKey(order.getTime())){
			System.out.println("Comanda de adaugat exista deja!");
			return false;
		}
		
		//IAU DIN DEPOZIT
		for(Product prod:order.getOrderedProducts()){
			if(!(warehouse.takeProd(prod, prod.getProdCant()))){
				System.out.println("UNDERSTOCK la adaugarea comenzii!");
				return false;
			}
		}
		System.out.println("Adaugarea comenzii s-a realizat cu succes");
		orders.put(System.currentTimeMillis(),order );
		return true;
	}
	
	//EXECUTA COMANDA
	/**
	 * Metoda confirma executarea unei comenzi, luru realizat de admin si apeleaza mai apoi
	 * metoda de generare a facturii.
	 * @return true sau false dupa cum s-a executat sau nu comanda. Ultimul caz 
	 * este valabil atunci cand nu exista nicio comanda de confirmat, adica arborele nu contine
	 * niciun element.
	 */
	public boolean commitOrder(){
		if (this.orders.size()==0){
			System.out.println("Nu exista comenzi de efectuat!");
			return false;
		}
		else{
			ordNr++;
			generateBill(orders.get(((TreeMap<Long, Order>) orders).firstKey()));
			orders.remove(((TreeMap<Long, Order>) orders).firstKey());
			System.out.println("Comanda s-a efectuat cu succes!");
			return true;
			
		}
	}
	
	
	
	/**
	 * Metoda genereaza o factura in care se descrie comanda curenta.
	 * @param order
	 */
	public void generateBill(Order order){
		StringBuilder fileName= new StringBuilder();
		fileName.append("Factura_comanda");
		fileName.append(Integer.toString(ordNr));
		fileName.append(".txt");
		PrintWriter writer;
		
		System.out.println(fileName);
		try {
			String updatedText = order.toString().replaceAll("\n", System.lineSeparator());
			writer = new PrintWriter(fileName.toString(), "UTF-8");
			writer.print(updatedText);
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	
	@Override
	public String toString() {
		return "OPDept [orders=" + orders + ", warehouse=" + warehouse + "]";
	}
	
	
	//GETTERS & SETTERS
	public Map<Long, Order> getOrders() {
		return orders;
	}
	public void setOrders(Map<Long, Order> orders) {
		this.orders = orders;
	}
	public Integer getOrdNr() {
		return ordNr;
	}
	public void setOrdNr(Integer ordNr) {
		this.ordNr = ordNr;
	}
	public Warehouse getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
}
