package model;
import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * Clasa Order reprezinta comanda. Ca si variabile instanta regasim o lista cu produsele comandate
 * in instanta curenta, clientul care la comandat, precum si timpul (in milisecunde) la care a fost
 * realizata comanda. Totodata, in cadrul casei se determina pretul total al comenzii.
 * 
 * 
 * @author Bogdan
 *
 */

public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private long time;
	private Double totalPrice;
	private Customer customer;
	private List<Product> orderedProducts= new ArrayList<Product>();
	
	
	
	//CONSTRUCTORS
	public Order(){
		time=System.currentTimeMillis();
	}
	public Order(List<Product> orderedProd, Customer customer){
		time=System.currentTimeMillis();
		this.orderedProducts=orderedProd;
		this.customer=customer;
		this.totalPrice=calcTotalPrice(orderedProducts);
		
	}
	
	/**
	 * Aceasta metoda calculeaza pretul total al produselor.Acest lucru se realizeaza
	 * prin parcurgerea listei de produse si insumarea pretului fiecaruia cu cantitatea comandata din acel
	 * produs.
	 * @param products lista de produse continuta in comanda curenta
	 * @return pretul total al comenzii, in urma adaugarii ei de catre client. Aceasta informatie
	 * va fi scrisa in factura.
	 */
	public Double calcTotalPrice(List<Product> products){
		Double totalPrice=0.0;
		for(Product prod: products){
			totalPrice+= (prod.getProdPrice()*prod.getProdCant());
		}
		return totalPrice;
	}
	/**
	 * Metoda to String ne ofera o situatie a comenzii, asa cum va fi ea scrisa in factura
	 * Ea contine toate informatiile despre client: nume, telefon, adresa, dar si legate de produsele
	 * comandate, cantitatea lor, pretul fiecaruia si pretul total.
	 * Factura va fi scrisa intr-un fisier .txt
	 */
	public String toString(){
		StringBuilder prodInfo = new StringBuilder();
		for(Product prod: orderedProducts){
			prodInfo.append("\t\tNume: "+prod.getProdName()+", cantitate: "+prod.getProdCant()+", pret: "+prod.getProdPrice()+";\n");
		}
		
		return ("Nume:"+customer.getName()+";\n"+
				"Telefon: "+customer.getTelephone()+";\n"+
				"Adresa: "+customer.getAdress()+";\n"+
				"Produse comandate: \n"+prodInfo+
				"Pret total: "+this.totalPrice+"."
				);
	}
	
	/**
	 * Aceasta metoda ne ofera o situatie a produselor din comanda curenta
	 * Asa cum vor fi ele scrise in tabelul de comenzi, accesibil administratorului.
	 * Motivarea folosirii e ei sta in faptul ca era nevoie, in acest nivel, de o reprezentare
	 * mai simplificata a comenzii decat cea notata in factura.
	 * @return
	 */
	public String simplifiedToString(){
		StringBuilder prodInfo = new StringBuilder();
		for(Product prod: orderedProducts){
			prodInfo.append("Nume: "+prod.getProdName()+", cantitate: "+prod.getProdCant()+", pret: "+prod.getProdPrice()+";\n");
		}
		return prodInfo.toString();
	}
	
	
	
	//GETTERS & SETTERS
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public List<Product> getOrderedProducts() {
		return orderedProducts;
	}
	public void setOrderedProducts(List<Product> orderedProducts) {
		this.orderedProducts = orderedProducts;
	}
	
	
	

}
