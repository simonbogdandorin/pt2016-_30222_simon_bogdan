package model;

import java.io.Serializable;
/**
 * 
 * @author Bogdan
 *Clasa Customer modeleaza clientul. Fata de admin, ea are variabile de instanta necesare pentru
 *identificarea utilizatorului si completarea campurilor facturii la adaugarea unei comenzi.
 *Acestea sunt: nume,adresa, telefon.
 *Un client are doar dreptul de a adauga produse intr-o comanda si a trimite comanda, fiind necesara
 *in prealabil o logare.
 *
 */
public class Customer  extends User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private String name;
	private String adress;
	private String telephone;
	
	
	//CONSTRUCTORS
	public Customer(String name, String username, String pass, String adress,String telephone){
		super(username,pass);
		this.name=name;
		this.adress=adress;
		this.telephone=telephone;
	}
	
	/**
	 * Metoda returneaza dreptul utilizatorului client, care este 1, deci un drept ce nu ii da
	 * acces in a opera cu produsele si cu ceilalti clienti.
	 */
	@Override
	public int getAcces(){
		return 1;
	}
	
	//GETTERS & SETTERS
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	

}
