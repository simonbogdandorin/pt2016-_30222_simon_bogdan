package model;

import java.io.Serializable;
/**
 * 
 * @author Bogdan
 * 
 * Clasa User este o clasa abstracta, fiind extinsa de clasele Admin si Customer. In consecinta nu putem
 * avea o instanta a ei, cele doua variante de utilizatori ai aplicatiei fiind client si admin
 * fiecare cu drepturile sale.
 * Totusi, aceste doua categorii au unele caracteristici comune, surprinse de clasa User cum ar fi
 * username-ul si parola necesare la logare
 * dar si un membru access care defineste accessul si deci dreptul utilizatorului curent.
 *
 */
public abstract class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	protected String username;
	protected String pass;
	protected int acces;
	
	//CONSTRUCTORS
 	public User(String username, String pass) {
		this.username = username;
		this.pass = pass;
	}
 	
 	/**
 	 * Metoda abstracta getAccess diferentiaza adminul de client.
 	 * @return dreptul utilizatorului curent.
 	 */
	abstract int getAcces();
	
	
	
	@Override
	public String toString() {
		return "User [username=" + username + ", pass=" + pass + "]";
	}

	//GETTERS & SETTERS
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
}
