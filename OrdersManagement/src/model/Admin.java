package model;

import java.io.Serializable;
/**
 * 
 * @author Bogdan
 *Clasa Admin extinde User si reprezinta un utilizator cu drepturi totale: de a adauga produse, de a modifica produse
 *, de a sterge produse. Aceleasi operatii se aplica si pe clienti.
 *Adminul tine evidenta comenzilor efectuate de clienti si le valideaza, generand totodata factura.
 */
public class Admin extends User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */


	//CONSTRUCTORS
	public Admin(String username, String pass){
		super(username,pass);
		
	}
	
	/**
	 * Metoda returneaza 0 reprezentand dreptul maxim ce-l poate avea un utilizator.
	 */
	@Override
	public int getAcces(){
		return 0;
	}

}
