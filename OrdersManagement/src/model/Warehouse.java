package model;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author Bogdan
 * Clasa modleaza un depozit. Ea contine un membru static ce reprezinta capacitatea totala a depozitului
 * orice incercare de depasire a acestia prin numarul produselor va fi semnalata adminului.
 * Totodata, clasa contine un arbore de produse, componente ale depozitului dar si o variabila
 * care retine numarul total de produse.
 *
 */
public class Warehouse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	public static final int CAPACITY=500;
	private Map<String,Product> products=new TreeMap<String,Product>();
	private Integer totalNumberOfProducts;
	
	//CONSTRUCTORS
	public Warehouse(){
		totalNumberOfProducts=0;
	}
	
	/**
	 * Metoda creaza un produs nou, lucru accesibil adminului.
	 * Ea contine validari legate de depasirea capacitatii depozitului prin 
	 * cantitatea indicata in cadrul parametrului precum si o testare a existentei in prealabil a produsului
	 * @param product produsul de adaugat
	 * @return true sau false dupa cum a reusit sau nu operatia
	 */
	public boolean createProd(Product product){
		if ((this.totalNumberOfProducts+product.getProdCant())>CAPACITY){
			System.out.println("OVERSTOCK la creare!");
			return false;
		}
		if(!this.products.containsKey(product.getProdId())){
			totalNumberOfProducts+=product.getProdCant();
			this.products.put(product.getProdId(), product);
			System.out.println("Produs creat cu succes!");
			return true;
		}
		else{
			System.out.println("Produsul de creat exista deja!");
			return false;
		}
	}
	
	/**
	 * Metoda adauga la cantitatea unui produs, cuprinzand totodata o serie de teste
	 * legate de depasirea capacitatii depozitului, precum si de existenta produsului la care
	 * se adauga.
	 * @param product Produsul la care se adauga
	 * @param cant Cantitatea de adaugat
	 * @return true sau false dupa cum a reusit sau nu operatia
	 */
	public boolean addExistingProd(Product product,int cant){
		if ((this.totalNumberOfProducts+cant)>CAPACITY){
			System.out.println("OVERSTOCK la adaugare!");
			return false;
		}
		if(this.products.containsKey(product.getProdId())){
			System.out.println("Produse adaugate cu succes!");
			totalNumberOfProducts+=cant;
			this.products.get(product.getProdId()).addCant(cant);
			return true;
		}
		else{
			System.out.println("Produsul nu exista!");
			return false;
		}
	}
	
	/**
	 * Metoda extrage din stocul depozitului o anumita cantitate dintr-un anumit produs. Totodata,
	 * testeaza daca produsul exista si daca poate fi exstrasa cantitatea dorita, iar cerinta
	 * nu depaseste resursele depozitului.
	 * @param product Produsul din care se vrea extragerea de cantitate
	 * @param cant Cantitatea dorita din produsul dat ca parametru
	 * @return true sau false dupa cum a reusit sau nu operatia
	 */
	public boolean takeProd(Product product,int cant){
		if ( ( ( this.products.get( product.getProdId() ).getProdCant())-cant)<0){
			System.out.println("UNDERSTOCK la luarea de produse!");
			return false;		
		}
		
		if(this.products.containsKey(product.getProdId())){
			totalNumberOfProducts-=cant;
			this.products.get(product.getProdId()).subCant(cant);
			System.out.println("Luarea de produse s-a realizat cu succes!");
			return true;
		}
		else{
			System.out.println("Produsul de luat nu a fost gasit!");
			return false;
		}
	}
	
	/**
	 * Metoda realizeaza stergerea unui produs, lucru accesibil adminului. Totodata
	 * , in cadrul ei se verifica daca produsul dat ca si parametru exista, in caz contrar,
	 * metoda returnand false.
	 * @param product Produsul care se vrea sters din depozit.
	 * @return true sau false dupa cum a reusit sau nu operatia.
	 */
	public boolean deleteProd(Product product){
		
		if (totalNumberOfProducts<0)
			totalNumberOfProducts=0;
		if(this.products.containsKey(product.getProdId())){
			System.out.println("Produsul a fost sters cu succes");
			totalNumberOfProducts-=product.getProdCant();
			this.products.remove(product.getProdId());
			return true;
		}
		else{
			System.out.println("Produsul de sters nu exista!");
			return false;
		}
	}
	
	
	/**
	 * Metoda modifica informatiile legate de produse, la cererea adminului.
	 * Lucrurile care pot fi modificare sunt cantitatea si pretul. Metoda testeaza
	 * daca nu se depaseste capacitatea depozitului la introducerea noii cantitati, caz in
	 * care returneaza false, dar si daca produsul de modificat exista.
	 * @param newProd O instanta a produsului noi.
	 * @return true sau false dupa cum a reusit sau nu operatia
	 */
	public boolean modifyProd(Product newProd){
		
		if(this.products.containsKey(newProd.getProdId())){
			if( (newProd.getProdCant())>CAPACITY){
				System.out.println("OVERSTOCK la modificarea produsului!");
				return false;
			}
			products.get(newProd.getProdId()).setProdCant(newProd.getProdCant());
			products.get(newProd.getProdId()).setProdPrice(newProd.getProdPrice());
			return true;
		}
		else{
			System.out.println("Produsul de modificat nu exista!");
			return false;
		}
	}
	
	
	
	
	
	@Override
	public String toString() {
		return "Warehouse [products=" + products + ", totalNumberOfProducts=" + totalNumberOfProducts + "]";
	}

	//GETTERS & SETTERS
	public Map<String, Product> getProducts() {
		return products;
	}
	public void setProducts(Map<String, Product> products) {
		this.products = products;
	}	
}
