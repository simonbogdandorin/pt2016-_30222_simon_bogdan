package model;

import java.io.Serializable;

/**
 * Clasa Product modeleaza produsul ce va fi componenta a arborelui binar de cautare incapsulat de depozit.
 * Clasa contine informatii legate de profus, informatii ce vor ajuta mai apoi la filtrare.
 * Totodata, clasa contine o variabila instanta numita prodCant, care tine evidenta numarului de produse existente in stoc.
 * Tot aici, am definit un set restrans de categorii posibile pentru produse, numit Category, pentru a nu mai exista, mai apoi, confuzii
 * legate de acest aspect, la introducerea de noi produse.
 * 
 * @author Bogdan
 *
 */

public class Product implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */


	public enum Category{
		ALIMENTE, ELECTROCASNICE, IMBRACAMINTE, MOBILIER, CONSTRUCTII,
		PAPETARIE, BAUTURI, COSMETICE, SPORT, IT
	}
	
	private String prodId;
	private String prodName;
	private Category prodCategory;
	private Double prodPrice;
	private Integer prodCant;
	
	//CONSTRUCTORS
	public Product(){
		
	}
	
	public Product(String name, Category cat, Double price, Integer cant){
		this.prodName=name;
		this.prodCategory=cat;
		this.prodPrice=price;
		this.prodCant=cant;
		this.prodId= cat.toString().concat(name);
	}
	
	public Product(Object fields[]){
		this.prodCategory=Product.Category.valueOf((String)fields[0]);
		this.prodName=(String)fields[1];
		this.prodPrice=(Double)fields[2];
		try{
		this.prodCant= (Integer)(fields[3]);
		}catch(ClassCastException e){
			this.prodCant=Integer.parseInt((String)fields[3]);
		}
		this.prodId= this.prodCategory.toString().concat(this.prodName);
	}
	
	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", prodName=" + prodName + ", prodCategory=" + prodCategory
				+ ", prodPrice=" + prodPrice + ", prodCant=" + prodCant + "]\n";
	}
	
	/**
	 * Metoda adauga la cantitatea unui produs, ea nu se ocupa cu validari, acestea revenind casei Warehouse
	 * cea care administreaza stocul si contine un TreeMap de astfel de produse.
	 * Adaugarea de produse se face prin intermediul adminului, cel care are acest drept.
	 * @param x Cantitatea de adugat
	 */
	public void addCant(int x){
		this.prodCant+=x;
	}
	/**
	 * Metoda scade din cantitatea unui produs, ea nu se ocupa cu validari, acestea revenind casei Warehouse
	 * cea care administreaza stocul si contine un TreeMap de astfel de produse. Prin urmare, neexistand la 
	 * acest nivel un test de understock, daca cantitatea dupa executare e mai mica decat 0, ea va fi trecuta
	 * pe 0.
	 * Aceasta metoda va fi pusa in evidenta de client, cel care efectueaza comenzi sau admin, 
	 * atunci cand modifica informatiile legate de un produs.
	 * @param x Cantitatea de scazut
	 */
	public void subCant(int x){
		this.prodCant-=x;
		if(x<0) 
			x=0;
	}

	//GETTERS & SETTERS
	
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId=prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Category getProdCategory() {
		return prodCategory;
	}

	public void setProdCategory(Category prodCategory) {
		this.prodCategory = prodCategory;
	}

	public Double getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(Double prodPrice) {
		this.prodPrice = prodPrice;
	}

	public Integer getProdCant() {
		return prodCant;
	}

	public void setProdCant(Integer prodCant) {
		this.prodCant = prodCant;
	}	
}


