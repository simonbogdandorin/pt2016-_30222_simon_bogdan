package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.Product;
import model.Product.Category;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import java.awt.Font;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class TheView {
	//FRAME
	private JFrame frame;
	private CardLayout cardLayout;
	
	//SIGN IN PANEL
	private JPanel signInPanel;
	private JTextField userNameField;
	private JPasswordField passwordField;
	private JButton btnSignIn;
	
	// MAIN CUSTOMER PANEL
	JPanel mainCustomerPanel;
	private JScrollPane customerProductsScrollPane;
	private JTable customerProductsTable;
	private JScrollPane customerCrtOrderScrollPane;
	private JTable customerCrtOrdersTable;
	private JButton btnAdaugaLaComanda;
	private JButton btnTrimiteComanda;
	private JButton btnCustomerLogOut;
	private JComboBox<Product.Category> customerFilterComboBox;
	private JButton btnCustomerProductsFilter;
	
	//MAIN ADMIN PANEL
	private JPanel mainAdminPanel;
	private JButton btnAdminOrders;
	private JButton btnAdminProducts;
	private JButton btnAdminCustomers;
	private JButton btnAdminLogOut;
	
	//ADMIN ORDERS PANEL
	private JPanel adminOrdersPanel;
	private JScrollPane adminOrdersScrollPane;
	private JTable adminOrdersTable;
	private JButton btnAdminOrderConfirm;
	
	//ADMIN PRODUCTS PANEL
	private JPanel adminProductsPanel;
	private JScrollPane adminProductsScrollPane;
	private JTable adminProductsTable;
	private JButton btnAdminAddProduct;
	private JButton btnAdminConfirmProductChanges;
	private JButton btnAdminDeleteProduct;
	private JComboBox<Product.Category> adminProdCatComboBox;
	private JTextField adminProdNameTextField;
	private JTextField adminProdPriceTextField;
	private JTextField adminProdCantTextField;
	
	
	//ADMIN CUSTOMERS PANEL
	private JPanel adminCustomersPanel;
	private JTable adminCustomersTable;
	private JScrollPane adminCustomersScrollPane;
	private JButton btnAdminDeleteCustomer;
	private JButton btnAdminConfirmCustomerChanges;
	private JButton btnAdminAddCustomer;
	private JTextField adminCustomerUNTextField;
	private JTextField adminCustomerPassTextField;
	private JTextField adminCustomerNameTextField;
	private JTextField adminCustomerTelTextField;
	private JTextField adminCustomerAdrTextField;
	
	//NAVIGATION
	private JButton btnInapoi1;
	private JButton btnInapoi2;
	private JButton btnInapoi3;
	

	
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TheView window = new TheView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TheView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		/* ***** FRAME ***** */
		frame = new JFrame();
		frame.setBounds(100, 100, 575, 414);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		frame.setVisible(true);
		

		/* ******* SIGN IN PANEL ******* */
		
		//SIGN IN PANE & LAYOUT
		signInPanel = new JPanel();
		frame.getContentPane().add(signInPanel, "signInPanel");
		signInPanel.setLayout(null);
		//SIGN IN BTN & ADD TO SIGN IN PANEL
		btnSignIn = new JButton("Sign in");
		btnSignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSignIn.setBounds(239, 190, 104, 28);
		signInPanel.add(btnSignIn);
		//USERNAME FIELD & PASS FIELD &LABELS
		userNameField = new JTextField();
		userNameField.setBounds(219, 128, 137, 20);
		signInPanel.add(userNameField);
		userNameField.setColumns(10);
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(151, 131, 68, 14);
		signInPanel.add(lblUsername);
		passwordField = new JPasswordField();
		passwordField.setBounds(219, 159, 137, 20);
		signInPanel.add(passwordField);
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(151, 162, 61, 14);
		signInPanel.add(lblPassword);
		
		
		
		/* ***** MAIN CUSTOMER PANEL ***** */
		
		
		
		//PANEL
		mainCustomerPanel= new JPanel();
		frame.getContentPane().add(mainCustomerPanel, "mainCustomerPanel");
		mainCustomerPanel.setLayout(null);
		//PRODUCTS SCROLL PANE
		customerProductsScrollPane=new JScrollPane();
		customerProductsScrollPane.setEnabled(false);
		customerProductsScrollPane.setSize(559, 108);
		customerProductsScrollPane.setLocation(0, 22);
		//CUSTOMER PRODUCTS TABLE
		DefaultTableModel tableModel= new DefaultTableModel(){
			      public boolean isCellEditable(int rowIndex, int mColIndex) {
			        return false;
			      }
		};
		customerProductsTable=new JTable(tableModel);
		customerProductsTable.setSelectionForeground(Color.WHITE);
		customerProductsTable.setSelectionBackground (Color.LIGHT_GRAY);
		customerProductsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		customerProductsTable.setRowSelectionAllowed(true);
		customerProductsTable.setSize(414, 80);
		customerProductsTable.setLocation(20, 52);
		customerProductsScrollPane.setViewportView(customerProductsTable);
		mainCustomerPanel.add(customerProductsScrollPane);
		//CUSTOMER CURRENT ORDER SCROLL PANE
		customerCrtOrderScrollPane=new JScrollPane();
		customerCrtOrderScrollPane.setSize(559, 113);
		customerCrtOrderScrollPane.setLocation(0, 262);
		//CUSTOMER CURRENT ORDER TABLE
		DefaultTableModel tableModel2= new DefaultTableModel(){
		      public boolean isCellEditable(int rowIndex, int mColIndex) {
			      if (mColIndex==3) 
			    	  return true;
			      else
			    	  return false;
			      }
		};
		
	    customerCrtOrdersTable=new JTable(tableModel2);
	    customerCrtOrdersTable.setSize(new Dimension(100, 100));
	    customerCrtOrdersTable.setShowVerticalLines(false);
	    customerCrtOrdersTable.setSelectionForeground(Color.WHITE);
	    customerCrtOrdersTable.setSelectionBackground (Color.LIGHT_GRAY);
	    customerCrtOrdersTable.setSize(559, 113);
	    customerCrtOrdersTable.setLocation(20,300);
	    customerCrtOrderScrollPane.setViewportView(customerCrtOrdersTable);
		mainCustomerPanel.add(customerCrtOrderScrollPane);
		//BTN ADAUGA LA COMANDA, BTN TRIMITE COMANDA, BTN FILTREAZA
		btnAdaugaLaComanda = new JButton("Adauga produs");
		btnAdaugaLaComanda.setBounds(10, 189, 116, 23);
		mainCustomerPanel.add(btnAdaugaLaComanda);
		btnTrimiteComanda = new JButton("Trimite comanda");
		btnTrimiteComanda.setBounds(215, 189, 116, 23);
		mainCustomerPanel.add(btnTrimiteComanda);
		btnCustomerLogOut = new JButton("Logout");
		btnCustomerLogOut.setBounds(443, 189, 116, 23);
		mainCustomerPanel.add(btnCustomerLogOut);
		btnCustomerProductsFilter = new JButton("Filtreaza");
		btnCustomerProductsFilter.setBounds(10, 141, 116, 20);
		mainCustomerPanel.add(btnCustomerProductsFilter);
		//FILTER COMBO BOX
		customerFilterComboBox= new JComboBox<Product.Category>();
		customerFilterComboBox.setBounds(215, 141, 116, 20);
		mainCustomerPanel.add(customerFilterComboBox);
		//LABELS
		JLabel lblproduse = new JLabel("~PRODUSE~");
		lblproduse.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblproduse.setBounds(227, -1, 116, 24);
		mainCustomerPanel.add(lblproduse);
		JLabel lblcomandaCurenta = new JLabel("~COMANDA CURENTA~");
		lblcomandaCurenta.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblcomandaCurenta.setBounds(187, 236, 194, 24);
		mainCustomerPanel.add(lblcomandaCurenta);
		
		
		
		
		
		/* ***** MAIN ADMIN PANEL ***** */
		
		//PANEL
		mainAdminPanel = new JPanel();
		frame.getContentPane().add(mainAdminPanel, "mainAdminPanel");
		mainAdminPanel.setLayout(null);
		//BUTTONS
		btnAdminOrders = new JButton("Comenzi");
		btnAdminOrders.setBounds(232, 99, 104, 40);
		mainAdminPanel.add(btnAdminOrders);
		btnAdminProducts = new JButton("Produse");
		btnAdminProducts.setBounds(232, 140, 104, 40);
		mainAdminPanel.add(btnAdminProducts);
		btnAdminCustomers = new JButton("Clienti");
		btnAdminCustomers.setBounds(232, 180, 104, 40);
		mainAdminPanel.add(btnAdminCustomers);
		btnAdminLogOut = new JButton("Logout");
		btnAdminLogOut.setBounds(232, 222, 104, 40);
		mainAdminPanel.add(btnAdminLogOut);
		
		
		/* **** ADMIN ORDERS PANEL **** */
		
		//PANEL COMENZI & SCROLL PANE & TABLE
		adminOrdersPanel = new JPanel();
		frame.getContentPane().add(adminOrdersPanel, "adminOrdersPanel");
		adminOrdersScrollPane=new JScrollPane();
		adminOrdersScrollPane.setEnabled(false);
		adminOrdersScrollPane.setSize(559, 108);
		adminOrdersScrollPane.setLocation(0,22);
		DefaultTableModel tableModel3= new DefaultTableModel(){
		      public boolean isCellEditable(int rowIndex, int mColIndex) {
			      return false;
			      }
		};
		
		adminOrdersTable = new JTable(tableModel3);
		adminOrdersTable.setEnabled(false);
		adminOrdersTable.setSize(414, 80);
		adminOrdersTable.setLocation(20,52);
		adminOrdersTable.setVisible(true);
		adminOrdersScrollPane.setViewportView(adminOrdersTable);
		adminOrdersPanel.setLayout(null);
		adminOrdersPanel.add(adminOrdersScrollPane);
		//BTN: INAPOI & ORDER CONFIRM 
		btnInapoi1 = new JButton("Inapoi la meniu");
		btnInapoi1.setBounds(340, 141, 111, 43);
		adminOrdersPanel.add(btnInapoi1);
		btnAdminOrderConfirm = new JButton("Confirma comanda");
		btnAdminOrderConfirm.setBounds(102, 141, 121, 43);
		adminOrdersPanel.add(btnAdminOrderConfirm);
		
		/* ***** ADMIN PRODUCTS PANEL **** */
		//PANEL
		adminProductsPanel = new JPanel();
		frame.getContentPane().add(adminProductsPanel, "adminProductsPanel");
		
		//SCROLL PANE & TABLE
		adminProductsScrollPane=new JScrollPane();
		adminProductsScrollPane.setEnabled(false);
		adminProductsScrollPane.setSize(559, 108);
		adminProductsScrollPane.setLocation(0,22);
		DefaultTableModel tableModel4= new DefaultTableModel(){
		      public boolean isCellEditable(int rowIndex, int mColIndex) {
			      if((mColIndex==0 )|| (mColIndex==1))
			    	  return false;
			      else
			    	  return true;
			      }
		};
		adminProductsTable = new JTable(tableModel4);
		adminProductsTable.setSize(414, 80);
		adminProductsTable.setLocation(20,52);
		adminProductsTable.setVisible(true);
		adminProductsScrollPane.setViewportView(adminProductsTable);
		adminProductsPanel.setLayout(null);
		adminProductsPanel.add(adminProductsScrollPane);
		//BTN : INAPOI & ADD PRODUCT & DELETE PRODUCT 
		btnInapoi2 = new JButton("Inapoi la meniu");
		btnInapoi2.setBounds(432, 153, 127, 32);
		adminProductsPanel.add(btnInapoi2);
		btnAdminAddProduct = new JButton("Adauga produs");
		btnAdminAddProduct.setBounds(0, 153, 138, 32);
		adminProductsPanel.add( btnAdminAddProduct);
		btnAdminConfirmProductChanges = new JButton("Confirma modificarile");
		btnAdminConfirmProductChanges.setBounds(285, 153, 138, 32);
		adminProductsPanel.add(btnAdminConfirmProductChanges);
		btnAdminDeleteProduct = new JButton("Sterge produs");
		btnAdminDeleteProduct.setBounds(148, 153, 127, 32);
		adminProductsPanel.add(btnAdminDeleteProduct);
		// TEXT FIELDS & COMBO BOX
		adminProdNameTextField = new JTextField();
		adminProdNameTextField.setBounds(166, 267, 86, 20);
		adminProductsPanel.add(adminProdNameTextField);
		adminProdNameTextField.setColumns(10);
		adminProdPriceTextField = new JTextField();
		adminProdPriceTextField.setBounds(345, 267, 46, 20);
		adminProductsPanel.add(adminProdPriceTextField);
		adminProdPriceTextField.setColumns(10);
		adminProdCantTextField = new JTextField();
		adminProdCantTextField.setBounds(503, 267, 40, 20);
		adminProductsPanel.add(adminProdCantTextField);
		adminProdCantTextField.setColumns(10);
		adminProdCatComboBox = new JComboBox<Product.Category>();
		adminProdCatComboBox.setBounds(0, 267, 106, 20);
		adminProductsPanel.add(adminProdCatComboBox);
		//LABELS
		JLabel lblCategorie = new JLabel("Categorie");
		lblCategorie.setBounds(10, 253, 50, 14);
		adminProductsPanel.add(lblCategorie);
		JLabel lblNewLabel_1 = new JLabel("Nume");
		lblNewLabel_1.setBounds(170, 253, 46, 14);
		adminProductsPanel.add(lblNewLabel_1);
		JLabel lblNewLabel_2 = new JLabel("Pret");
		lblNewLabel_2.setBounds(345, 253, 46, 14);
		adminProductsPanel.add(lblNewLabel_2);
		JLabel lblNewLabel_3 = new JLabel("Cantitate");
		lblNewLabel_3.setBounds(503, 253, 46, 14);
		adminProductsPanel.add(lblNewLabel_3);
		
		/* **** ADMIN CUSTOMERS PANEL **** */
		
		//PANEL
		adminCustomersPanel = new JPanel();
		frame.getContentPane().add(adminCustomersPanel, "adminCustomersPanel");
		//SCROLL PANE & TABEL
		adminCustomersScrollPane=new JScrollPane();
		adminCustomersScrollPane.setEnabled(false);
		adminCustomersScrollPane.setSize(559, 108);
		adminCustomersScrollPane.setLocation(0,22);
		DefaultTableModel tableModel5= new DefaultTableModel(){
		      public boolean isCellEditable(int rowIndex, int mColIndex) {
		    	  if ((mColIndex==1)||(mColIndex==0))
		    		  return false;
		    	  else
		    		  return true;
			      }
		};
		adminCustomersTable = new JTable(tableModel5);
		adminCustomersTable.setEnabled(true);
		adminCustomersTable.setSize(414, 80);
		adminCustomersTable.setLocation(20,52);
		adminCustomersTable.setVisible(true);
		adminCustomersScrollPane.setViewportView(adminCustomersTable);
		adminCustomersPanel.setLayout(null);
		adminCustomersPanel.add(adminCustomersScrollPane);
		//BTN: ADD CUSTOMER & CONFIRM CHANGES & DELETE CUSTOMER & INAPOI
		btnInapoi3 = new JButton("Inapoi la meniu");
		btnInapoi3.setBounds(428, 141, 131, 27);
		adminCustomersPanel.add(btnInapoi3);
		btnAdminAddCustomer = new JButton("Adauga client");
		btnAdminAddCustomer.setBounds(0, 141, 130, 29);
		adminCustomersPanel.add(btnAdminAddCustomer);
		btnAdminConfirmCustomerChanges = new JButton("Confirma modificarile");
		btnAdminConfirmCustomerChanges.setBounds(287, 141, 131, 29);
		adminCustomersPanel.add(btnAdminConfirmCustomerChanges);
		btnAdminDeleteCustomer = new JButton("Sterge client");
		btnAdminDeleteCustomer.setBounds(140, 141, 131, 29);
		adminCustomersPanel.add(btnAdminDeleteCustomer);
		//ADD CUSTOMERS TEXT FIELDS
		adminCustomerUNTextField = new JTextField();
		adminCustomerUNTextField.setBounds(10, 296, 86, 20);
		adminCustomersPanel.add(adminCustomerUNTextField);
		adminCustomerUNTextField.setColumns(10);
		adminCustomerPassTextField = new JTextField();
		adminCustomerPassTextField.setBounds(140, 296, 86, 20);
		adminCustomersPanel.add(adminCustomerPassTextField);
		adminCustomerPassTextField.setColumns(10);
		adminCustomerNameTextField = new JTextField();
		adminCustomerNameTextField.setBounds(259, 296, 86, 20);
		adminCustomersPanel.add(adminCustomerNameTextField);
		adminCustomerNameTextField.setColumns(10);
		adminCustomerTelTextField = new JTextField();
		adminCustomerTelTextField.setBounds(372, 296, 86, 20);
		adminCustomersPanel.add(adminCustomerTelTextField);
		adminCustomerTelTextField.setColumns(10);
		adminCustomerAdrTextField = new JTextField();
		adminCustomerAdrTextField.setBounds(473, 296, 86, 20);
		adminCustomersPanel.add(adminCustomerAdrTextField);
		adminCustomerAdrTextField.setColumns(10);
		//LABELS
		JLabel lblUsername_1 = new JLabel("Username");
		lblUsername_1.setBounds(10, 282, 53, 14);
		adminCustomersPanel.add(lblUsername_1);
		JLabel lblNewLabel_4 = new JLabel("Parola");
		lblNewLabel_4.setBounds(149, 282, 46, 14);
		adminCustomersPanel.add(lblNewLabel_4);
		JLabel lblAdresa = new JLabel("Adresa");
		lblAdresa.setBounds(503, 282, 46, 14);
		adminCustomersPanel.add(lblAdresa);
		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setBounds(372, 282, 46, 14);
		adminCustomersPanel.add(lblTelefon);
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(259, 282, 46, 14);
		adminCustomersPanel.add(lblNume);
		
		cardLayout=(CardLayout) frame.getContentPane().getLayout();
		cardLayout.first(frame.getContentPane());
	}
	
	public void displayMessageDialog(String message){
		JOptionPane.showMessageDialog(frame, message);
		}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public CardLayout getCardLayout() {
		return cardLayout;
	}

	public void setCardLayout(CardLayout cardLayout) {
		this.cardLayout = cardLayout;
	}

	public JPanel getSignInPanel() {
		return signInPanel;
	}

	public void setSignInPanel(JPanel signInPanel) {
		this.signInPanel = signInPanel;
	}

	public JTextField getUserNameField() {
		return userNameField;
	}

	public void setUserNameField(JTextField userNameField) {
		this.userNameField = userNameField;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}

	public JButton getBtnSignIn() {
		return btnSignIn;
	}

	public void setBtnSignIn(JButton btnSignIn) {
		this.btnSignIn = btnSignIn;
	}

	public JScrollPane getcustomerProductsScrollPane() {
		return customerProductsScrollPane;
	}

	public void setCustomerProductsScrollPane(JScrollPane customerProductsScrollPane) {
		this.customerProductsScrollPane = customerProductsScrollPane;
	}

	public JScrollPane getCustomerCrtOrderScrollPane() {
		return customerCrtOrderScrollPane;
	}

	public void setCustomerCrtOrderScrollPane(JScrollPane customerProductsScrollPane2) {
		this.customerCrtOrderScrollPane = customerProductsScrollPane2;
	}

	public JTable getTable() {
		return customerProductsTable;
	}

	public void setTable(JTable table) {
		this.customerProductsTable = table;
	}

	public JTable getCustomerCrtOrdersTable() {
		return customerCrtOrdersTable;
	}

	public void setCustomeCrtOrdersTable(JTable ordersTable) {
		this.customerCrtOrdersTable = ordersTable;
	}

	public JTextField getTextField_1() {
		return adminProdNameTextField;
	}

	public void setTextField_1(JTextField textField_1) {
		this.adminProdNameTextField = textField_1;
	}

	public JTextField getTextField_2() {
		return adminProdPriceTextField;
	}

	public void setTextField_2(JTextField textField_2) {
		this.adminProdPriceTextField = textField_2;
	}

	public JTextField getTextField_3() {
		return adminProdCantTextField;
	}

	public void setTextField_3(JTextField textField_3) {
		this.adminProdCantTextField = textField_3;
	}

	public JScrollPane getCustomersAdminScrollPane() {
		return adminCustomersScrollPane;
	}

	public void setCustomersAdminScrollPane(JScrollPane customersAdminScrollPane) {
		this.adminCustomersScrollPane = customersAdminScrollPane;
	}

	public JTable getCustomersAdminTable() {
		return adminCustomersTable;
	}

	public void setCustomersAdminTable(JTable customersAdminTable) {
		this.adminCustomersTable = customersAdminTable;
	}

	public JTextField getTextField_4() {
		return adminCustomerUNTextField;
	}

	public void setTextField_4(JTextField textField_4) {
		this.adminCustomerUNTextField = textField_4;
	}

	public JTextField getTextField_5() {
		return adminCustomerPassTextField;
	}

	public void setTextField_5(JTextField textField_5) {
		this.adminCustomerPassTextField = textField_5;
	}

	public JTextField getTextField_6() {
		return adminCustomerNameTextField;
	}

	public void setTextField_6(JTextField textField_6) {
		this.adminCustomerNameTextField = textField_6;
	}

	public JTextField getTextField_7() {
		return adminCustomerTelTextField;
	}

	public void setTextField_7(JTextField textField_7) {
		this.adminCustomerTelTextField = textField_7;
	}

	public JTextField getTextField_8() {
		return adminCustomerAdrTextField;
	}

	public void setTextField_8(JTextField textField_8) {
		this.adminCustomerAdrTextField = textField_8;
	}

	public JPanel getMainCustomerPanel() {
		return mainCustomerPanel;
	}

	public void setMainCustomerPanel(JPanel mainCustomerPanel) {
		this.mainCustomerPanel = mainCustomerPanel;
	}

	public JTable getCustomerProductsTable() {
		return customerProductsTable;
	}

	public void setCustomerProductsTable(JTable customerProductsTable) {
		this.customerProductsTable = customerProductsTable;
	}

	public JButton getBtnAdaugaLaComanda() {
		return btnAdaugaLaComanda;
	}

	public void setBtnAdaugaLaComanda(JButton btnAdaugaLaComanda) {
		this.btnAdaugaLaComanda = btnAdaugaLaComanda;
	}

	public JButton getBtnCustomerLogOut() {
		return btnCustomerLogOut;
	}

	public void setBtnCustomerLogOut(JButton btnCustomerLogOut) {
		this.btnCustomerLogOut = btnCustomerLogOut;
	}

	public JComboBox<Category> getCustomerFilterComboBox() {
		return customerFilterComboBox;
	}

	public void setCustomerFilterComboBox(JComboBox<Category> customerFilterComboBox) {
		this.customerFilterComboBox = customerFilterComboBox;
	}

	public JScrollPane getCustomerProductsScrollPane() {
		return customerProductsScrollPane;
	}

	public void setCustomerCrtOrdersTable(JTable customerCrtOrdersTable) {
		this.customerCrtOrdersTable = customerCrtOrdersTable;
	}

	public JPanel getMainAdminPanel() {
		return mainAdminPanel;
	}

	public void setMainAdminPanel(JPanel mainAdminPanel) {
		this.mainAdminPanel = mainAdminPanel;
	}

	public JButton getBtnAdminOrders() {
		return btnAdminOrders;
	}

	public void setBtnAdminOrders(JButton btnAdminOrders) {
		this.btnAdminOrders = btnAdminOrders;
	}

	public JButton getBtnAdminProducts() {
		return btnAdminProducts;
	}

	public void setBtnAdminProducts(JButton btnAdminProducts) {
		this.btnAdminProducts = btnAdminProducts;
	}

	public JButton getBtnAdminCustomers() {
		return btnAdminCustomers;
	}

	public void setBtnAdminCustomers(JButton btnAdminCustomers) {
		this.btnAdminCustomers = btnAdminCustomers;
	}

	public JButton getBtnAdminLogOut() {
		return btnAdminLogOut;
	}

	public void setBtnAdminLogOut(JButton btnAdminLogOut) {
		this.btnAdminLogOut = btnAdminLogOut;
	}

	public JButton getBtnInapoi1() {
		return btnInapoi1;
	}

	public void setBtnInapoi1(JButton btnInapoi1) {
		this.btnInapoi1 = btnInapoi1;
	}

	public JButton getBtnInapoi2() {
		return btnInapoi2;
	}

	public void setBtnInapoi2(JButton btnInapoi2) {
		this.btnInapoi2 = btnInapoi2;
	}

	public JButton getBtnInapoi3() {
		return btnInapoi3;
	}

	public void setBtnInapoi3(JButton btnInapoi3) {
		this.btnInapoi3 = btnInapoi3;
	}

	public JButton getBtnTrimiteComanda() {
		return btnTrimiteComanda;
	}

	public void setBtnTrimiteComanda(JButton btnTrimiteComanda) {
		this.btnTrimiteComanda = btnTrimiteComanda;
	}

	public JButton getBtnCustomerProductsFilter() {
		return btnCustomerProductsFilter;
	}

	public void setBtnCustomerProductsFilter(JButton btnCustomerProductsFilter) {
		this.btnCustomerProductsFilter = btnCustomerProductsFilter;
	}

	public JPanel getAdminOrdersPanel() {
		return adminOrdersPanel;
	}

	public void setAdminOrdersPanel(JPanel adminOrdersPanel) {
		this.adminOrdersPanel = adminOrdersPanel;
	}

	public JScrollPane getAdminOrdersScrollPane() {
		return adminOrdersScrollPane;
	}

	public void setAdminOrdersScrollPane(JScrollPane adminOrdersScrollPane) {
		this.adminOrdersScrollPane = adminOrdersScrollPane;
	}

	public JTable getAdminOrdersTable() {
		return adminOrdersTable;
	}

	public void setAdminOrdersTable(JTable adminOrdersTable) {
		this.adminOrdersTable = adminOrdersTable;
	}

	public JButton getBtnAdminOrderConfirm() {
		return btnAdminOrderConfirm;
	}

	public void setBtnAdminOrderConfirm(JButton btnAdminOrderConfirm) {
		this.btnAdminOrderConfirm = btnAdminOrderConfirm;
	}

	public JPanel getAdminProductsPanel() {
		return adminProductsPanel;
	}

	public void setAdminProductsPanel(JPanel adminProductsPanel) {
		this.adminProductsPanel = adminProductsPanel;
	}

	public JScrollPane getAdminProductsScrollPane() {
		return adminProductsScrollPane;
	}

	public void setAdminProductsScrollPane(JScrollPane adminProductsScrollPane) {
		this.adminProductsScrollPane = adminProductsScrollPane;
	}

	public JTable getAdminProductsTable() {
		return adminProductsTable;
	}

	public void setAdminProductsTable(JTable adminProductsTable) {
		this.adminProductsTable = adminProductsTable;
	}

	public JButton getBtnAdminAddProduct() {
		return btnAdminAddProduct;
	}

	public void setBtnAdminAddProduct(JButton btnAdminAddProduct) {
		this.btnAdminAddProduct = btnAdminAddProduct;
	}

	public JButton getBtnAdminConfirmProductChanges() {
		return btnAdminConfirmProductChanges;
	}

	public void setBtnAdminConfirmProductChanges(JButton btnAdminConfirmProductChanges) {
		this.btnAdminConfirmProductChanges = btnAdminConfirmProductChanges;
	}

	public JComboBox<Product.Category> getAdminProdCatComboBox() {
		return adminProdCatComboBox;
	}

	public void setAdminProdCatComboBox(JComboBox<Product.Category> adminProdCatComboBox) {
		this.adminProdCatComboBox = adminProdCatComboBox;
	}

	public JTextField getAdminProdNameTextField() {
		return adminProdNameTextField;
	}

	public void setAdminProdNameTextField(JTextField adminProdNameTextField) {
		this.adminProdNameTextField = adminProdNameTextField;
	}

	public JTextField getAdminProdPriceTextField() {
		return adminProdPriceTextField;
	}

	public void setAdminProdPriceTextField(JTextField adminProdPriceTextField) {
		this.adminProdPriceTextField = adminProdPriceTextField;
	}

	public JTextField getAdminProdCantTextField() {
		return adminProdCantTextField;
	}

	public void setAdminProdCantTextField(JTextField adminProdCantTextField) {
		this.adminProdCantTextField = adminProdCantTextField;
	}

	public JButton getBtnAdminDeleteProduct() {
		return btnAdminDeleteProduct;
	}

	public void setBtnAdminDeleteProduct(JButton btnAdminDeleteProduct) {
		this.btnAdminDeleteProduct = btnAdminDeleteProduct;
	}

	public JPanel getAdminCustomersPanel() {
		return adminCustomersPanel;
	}

	public void setAdminCustomersPanel(JPanel adminCustomersPanel) {
		this.adminCustomersPanel = adminCustomersPanel;
	}

	public JTable getAdminCustomersTable() {
		return adminCustomersTable;
	}

	public void setAdminCustomersTable(JTable adminCustomersTable) {
		this.adminCustomersTable = adminCustomersTable;
	}

	public JScrollPane getAdminCustomersScrollPane() {
		return adminCustomersScrollPane;
	}

	public void setAdminCustomersScrollPane(JScrollPane adminCustomersScrollPane) {
		this.adminCustomersScrollPane = adminCustomersScrollPane;
	}

	public JButton getBtnAdminDeleteCustomer() {
		return btnAdminDeleteCustomer;
	}

	public void setBtnAdminDeleteCustomer(JButton btnAdminDeleteCustomer) {
		this.btnAdminDeleteCustomer = btnAdminDeleteCustomer;
	}

	public JButton getBtnAdminConfirmCustomerChanges() {
		return btnAdminConfirmCustomerChanges;
	}

	public void setBtnAdminConfirmCustomerChanges(JButton btnAdminConfirmCustomerChanges) {
		this.btnAdminConfirmCustomerChanges = btnAdminConfirmCustomerChanges;
	}

	public JButton getBtnAdminAddCustomer() {
		return btnAdminAddCustomer;
	}

	public void setBtnAdminAddCustomer(JButton btnAdminAddCustomer) {
		this.btnAdminAddCustomer = btnAdminAddCustomer;
	}

	public JTextField getAdminCustomerUNTextField() {
		return adminCustomerUNTextField;
	}

	public void setAdminCustomerUNTextField(JTextField adminCustomerUNTextField) {
		this.adminCustomerUNTextField = adminCustomerUNTextField;
	}

	public JTextField getAdminCustomerPassTextField() {
		return adminCustomerPassTextField;
	}

	public void setAdminCustomerPassTextField(JTextField adminCustomerPassTextField) {
		this.adminCustomerPassTextField = adminCustomerPassTextField;
	}

	public JTextField getAdminCustomerNameTextField() {
		return adminCustomerNameTextField;
	}

	public void setAdminCustomerNameTextField(JTextField adminCustomerNameTextField) {
		this.adminCustomerNameTextField = adminCustomerNameTextField;
	}

	public JTextField getAdminCustomerTelTextField() {
		return adminCustomerTelTextField;
	}

	public void setAdminCustomerTelTextField(JTextField adminCustomerTelTextField) {
		this.adminCustomerTelTextField = adminCustomerTelTextField;
	}

	public JTextField getAdminCustomerAdrTextField() {
		return adminCustomerAdrTextField;
	}

	public void setAdminCustomerAdrTextField(JTextField adminCustomerAdrTextField) {
		this.adminCustomerAdrTextField = adminCustomerAdrTextField;
	}
	
	
}
