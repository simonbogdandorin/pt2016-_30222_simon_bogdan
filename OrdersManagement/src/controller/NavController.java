package controller;
import model.Admin;
import model.Product;
import model.TheModel;
import model.User;
import model.Warehouse;
import view.TheView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

public class NavController {
	public static User crtUser;
	
	private TheView theView;
	private TheModel theModel;
	private ActionListener signInActionListener;
	private ActionListener logOutActionListener;
	private ActionListener backActionListener;
	
	
	
	//CONSTRUCTORS
	public NavController(TheModel model,TheView view){
		this.theModel=model;
		this.theView=view;
	
	}

	
	public void init(){
		crtUser=theModel.getUsersManager().getUsers().get(1);
		//SIGN IN
		signInActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				verifSignIn();
				if (crtUser!=null){
					setMainPanel();
				}
				else {
					theView.displayMessageDialog("Utilizatorul nu exista!");
					theView.getPasswordField().setText("");
					theView.getUserNameField().setText("");
				}
				
			}
			};;
		
		//LOG OUT	
		logOutActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				theView.getPasswordField().setText("");
				theView.getUserNameField().setText("");
				((DefaultTableModel)theView.getCustomerCrtOrdersTable().getModel()).setRowCount(0);
				theView.getCardLayout().show(theView.getFrame().getContentPane(), "signInPanel");	
			}
			};;
			
		//BACK
		backActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				theView.getCardLayout().show(theView.getFrame().getContentPane(), "mainAdminPanel");	
			}
			};;
		
		
		theView.getBtnAdminLogOut().addActionListener(logOutActionListener);
		theView.getBtnCustomerLogOut().addActionListener(logOutActionListener);	
		theView.getBtnSignIn().addActionListener(signInActionListener);
		theView.getBtnInapoi1().addActionListener(backActionListener);
		theView.getBtnInapoi2().addActionListener(backActionListener);
		theView.getBtnInapoi3().addActionListener(backActionListener);
		
	}

	public void verifSignIn(){
		String uname= theView.getUserNameField().getText();
		String pass=new String(theView.getPasswordField().getPassword());
		crtUser=theModel.getUsersManager().getUser(uname,pass);
		
	}
	
	public void setMainPanel(){
		
		if(crtUser instanceof Admin){
			theView.getCardLayout().show(theView.getFrame().getContentPane(), "mainAdminPanel");
		}
		else{
			initCustomerProductsTable(this.theModel.getOpdept().getWarehouse());
			initFilterComboBox();
			theView.getCardLayout().show(theView.getFrame().getContentPane(), "mainCustomerPanel");
		}
	}
	
	public void initCustomerProductsTable(Warehouse warehouse){
		DefaultTableModel model= (DefaultTableModel) theView.getCustomerProductsTable().getModel();
		model.setRowCount(0);
		for (Map.Entry<String,Product> entry : warehouse.getProducts().entrySet()){
		    model.addRow(new Object[] { entry.getValue().getProdCategory().toString(),
		    							entry.getValue().getProdName(),
		    							entry.getValue().getProdPrice(),
		    							entry.getValue().getProdCant()
		    							});
		}
	}
	
	public void initFilterComboBox(){
		theView.getCustomerFilterComboBox().addItem(Product.Category.ALIMENTE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.ELECTROCASNICE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.BAUTURI);
		theView.getCustomerFilterComboBox().addItem(Product.Category.IT);
		theView.getCustomerFilterComboBox().addItem(Product.Category.PAPETARIE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.COSMETICE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.IMBRACAMINTE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.MOBILIER);
		theView.getCustomerFilterComboBox().addItem(Product.Category.SPORT);
	}
	
	//GETTERS &SETTERS
	public User getCrtUser(){
		return crtUser;
	}
	
}
