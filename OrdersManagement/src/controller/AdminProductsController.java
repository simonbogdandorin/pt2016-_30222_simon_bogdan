package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;

import model.Product;
import model.TheModel;
import view.TheView;

public class AdminProductsController {
	private TheModel theModel = new TheModel();
	private TheView theView=new TheView();
	private ActionListener addProdActionListener;
	private ActionListener deleteProdActionListener;
	private ActionListener confirmChangesActionListener;
	
	
	public AdminProductsController(TheModel theModel,TheView theView){
		this.theModel=theModel;
		this.theView=theView;
	}
	
	public void init(){
		addProdActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
			//CREATE
				try{
				if (!theView.getAdminProdNameTextField().getText().equals(""))
					createProduct();
				else
					theView.displayMessageDialog("Introduceti toate campurile!");
				}catch(NumberFormatException e){
					theView.displayMessageDialog("Campuri invalide!");
				}
			}
			};
		theView.getBtnAdminAddProduct().addActionListener(addProdActionListener);
		
		confirmChangesActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
			//CHANGE
			
				confirmChanges();
				
			}
			};
		theView.getBtnAdminConfirmProductChanges().addActionListener(confirmChangesActionListener);
		
		deleteProdActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
			//DELETE
				deleteProduct();
				
			}
			};
		theView.getBtnAdminDeleteProduct().addActionListener(deleteProdActionListener);
	}
	
	public void createProduct(){
		
		Product.Category cat= (Product.Category)theView.getAdminProdCatComboBox().getSelectedItem();
		String name=theView.getAdminProdNameTextField().getText();
		
		Double price=Double.parseDouble(theView.getAdminProdPriceTextField().getText());
		Integer cant=Integer.parseInt(theView.getAdminProdCantTextField().getText());
		
		Product newProd= new Product(name,cat,price,cant);
		theModel.getOpdept().getWarehouse().createProd(newProd);
		DefaultTableModel model=(DefaultTableModel) theView.getAdminProductsTable().getModel();
		model.addRow(new Object []{cat,name,price,cant});
		
	}
	
	public void deleteProduct(){
		int selectedRow = theView.getAdminProductsTable().getSelectedRow();
		selectedRow = theView.getAdminProductsTable().convertRowIndexToModel(selectedRow);
		if(selectedRow==-1) {
			theView.displayMessageDialog("Niciun produs selectat!");
			return;
		}
		DefaultTableModel model = (DefaultTableModel)theView.getAdminProductsTable().getModel();
		int nrOfColumns=model.getColumnCount();
		Object[] fields=new Object[nrOfColumns];
		for(int i=0;i<nrOfColumns;i++){
			fields[i]=model.getValueAt(selectedRow,i);
		}
		Product product=new Product(fields);
		theModel.getOpdept().getWarehouse().deleteProd(product);
		model.removeRow(selectedRow);
		
	}
	public void confirmChanges(){
		int selectedRow = theView.getAdminProductsTable().getSelectedRow();
		selectedRow = theView.getAdminProductsTable().convertRowIndexToModel(selectedRow);
		if(selectedRow==-1) {
			theView.displayMessageDialog("Niciun produs selectat!");
			return;
		}
		DefaultTableModel model = (DefaultTableModel)theView.getAdminProductsTable().getModel();
		
		Product.Category cat= Product.Category.valueOf((String)model.getValueAt(selectedRow, 0));
		String name=(String) model.getValueAt(selectedRow, 1);
		Double price=(Double)model.getValueAt(selectedRow, 2);
		Integer cant;
		try{
			cant=(Integer)model.getValueAt(selectedRow, 3);
		}catch(ClassCastException e){
			cant=Integer.parseInt((String)model.getValueAt(selectedRow, 3));
		}
		Product product=new Product(name,cat,price,cant);
		if(!theModel.getOpdept().getWarehouse().modifyProd(product)){
			theView.displayMessageDialog("OVERSTOCK!");
		}
	}
	
}

