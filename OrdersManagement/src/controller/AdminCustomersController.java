package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;

import model.Customer;
import model.TheModel;
import view.TheView;

public class AdminCustomersController {
	private TheView theView;
	private TheModel theModel;
	private ActionListener addCustomerActionListener;
	private ActionListener modifyCustomerActionListener;
	private ActionListener deleteCustomerActionListener;
	
	public AdminCustomersController(TheModel theModel,TheView view){
		this.theView=view;
		this.theModel=theModel;
	}
	
	public void init(){
		addCustomerActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				//ADAUGA CLIENT
				addCustomer();
			}
			};
		theView.getBtnAdminAddCustomer().addActionListener(addCustomerActionListener);
		
		modifyCustomerActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				//MODIFICA CLIENT
				modifyCustomer();
			}
			};
		theView.getBtnAdminConfirmCustomerChanges().addActionListener(modifyCustomerActionListener);
		
		deleteCustomerActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				//STERGE CLIENT
				deleteCustomer();
			}
			};
		theView.getBtnAdminDeleteCustomer().addActionListener(deleteCustomerActionListener);
		
		
	}
	
	public void addCustomer(){
		String username=theView.getAdminCustomerUNTextField().getText();
		String pass=theView.getAdminCustomerPassTextField().getText();
		String name=theView.getAdminCustomerNameTextField().getText();
		String tel=theView.getAdminCustomerTelTextField().getText();
		String adress=theView.getAdminCustomerAdrTextField().getText();
		if (username.equals("")&&pass.equals("")&&name.equals("")&&name.equals("")
				&&tel.equals("")&&adress.equals("")){
			theView.displayMessageDialog("Toate campurile sunt obligatorii!");
			return;
			
		}
		
		Customer customer= new Customer(name,username,pass,adress,tel);
		if (theModel.getUsersManager().addCustomer(customer)){
			DefaultTableModel model = (DefaultTableModel)theView.getAdminCustomersTable().getModel();
			model.addRow(new Object []{username,pass,name,tel,adress});
		}
		else{
			theView.displayMessageDialog("Username-ul este deja inregistrat !");
			return;
		}
	}
	
	public void modifyCustomer(){
		DefaultTableModel model = (DefaultTableModel)theView.getAdminCustomersTable().getModel();
		int selectedRow=theView.getAdminCustomersTable().getSelectedRow();
		if(selectedRow==-1){
			theView.displayMessageDialog("Niciun rand nu a fost selectat !");
			return;
		}

		String username=(String)model.getValueAt(selectedRow, 0);
		String pass=(String)model.getValueAt(selectedRow, 1);
		String name=(String)model.getValueAt(selectedRow, 2);
		String tel=(String)model.getValueAt(selectedRow, 3);
		String adr=(String)model.getValueAt(selectedRow, 4);
		Customer customer= new Customer(name,username,pass,adr,tel);
		theModel.getUsersManager().modifyCustomer(customer);
		model.removeRow(selectedRow);
		model.addRow(new Object[]{
			username,pass,name,tel,adr
		});
		
		
	}
	
	public void deleteCustomer(){
		DefaultTableModel model = (DefaultTableModel)theView.getAdminCustomersTable().getModel();
		int selectedRow=theView.getAdminCustomersTable().getSelectedRow();
		if(selectedRow==-1){
			theView.displayMessageDialog("Niciun rand nu a fost selectat !");
			return;
		}
		String username=(String)model.getValueAt(selectedRow, 0);
		String pass=(String)model.getValueAt(selectedRow, 1);
		String name=(String)model.getValueAt(selectedRow, 2);
		String tel=(String)model.getValueAt(selectedRow, 3);
		String adr=(String)model.getValueAt(selectedRow, 4);
		
		Customer customer= new Customer(name,username,pass,adr,tel);
		
		theModel.getUsersManager().deleteCustomer(customer);
		model.removeRow(selectedRow);
		
		
	}
}


