package controller;




import javax.swing.table.DefaultTableModel;
import model.TheModel;
import view.TheView;

public class TheController {
	
	private TheModel theModel;
	private TheView theView;
	private NavController navController;
	private CustomerOperationsController customerOpController;
	private AdminOperationsController adminOpController;
	private AdminOrdersController adminOrdersController;
	private AdminProductsController adminProductsController;
	private AdminCustomersController adminCustomersController;
	
	//CONSTRUCTORS
	public TheController(TheModel theModel, TheView theView){
		this.theModel=theModel;
	    this.theView=theView;
	    navController=new NavController(theModel,theView);
	    customerOpController=new CustomerOperationsController(theModel,theView);
	    adminOrdersController = new AdminOrdersController(theModel,theView);
	    adminOpController=new AdminOperationsController(theModel,theView);
	    adminProductsController= new AdminProductsController(theModel,theView);
	    adminCustomersController = new AdminCustomersController(theModel,theView);
	    
	}
	
	public void init(){
		//theModel.initDummyVal();
		
		//INITIALIZE ALL THE CONTROLLERS
		navController.init();
		customerOpController.init();
		adminOpController.init();
		adminOrdersController.init();
		adminProductsController.init();
		adminCustomersController.init();
		
		//ADD COLUMNS TO THE TABLES
		initProductsTablesColumns();
		initOrdersTablesColumns();
		initCustomersTablesColumns();

		
		

		theView.getCardLayout().show(theView.getFrame().getContentPane(),"signInPanel");
		
		
	}
	
	public void initProductsTablesColumns(){
		String[] columnNames = {"CATEGORIE", "NUME","PRET","CANTITATE"};
		
		for(int i=0;i<4;i++){
			((DefaultTableModel)(theView.getCustomerProductsTable().getModel())).addColumn(columnNames[i]);
			((DefaultTableModel)(theView.getCustomerCrtOrdersTable().getModel())).addColumn(columnNames[i]);
			((DefaultTableModel)(theView.getAdminProductsTable().getModel())).addColumn(columnNames[i]);
		}
		theView.getCustomerProductsTable().repaint();
		theView.getCustomerCrtOrdersTable().repaint();
		theView.getAdminProductsTable().repaint();
		}
	
	public void initCustomersTablesColumns(){
		String[] columnNames = {"USERNAME", "PAROLA","NUME","TELEFON", "ADRESA"};
		
		for(int i=0;i<5;i++){
			((DefaultTableModel)(theView.getAdminCustomersTable().getModel())).addColumn(columnNames[i]);
		}
		
		theView.getAdminCustomersTable().repaint();
	}	
	
	
	public void initOrdersTablesColumns(){
		String[] columnNames = {"TIMP", "NUME","PRODUSE","PRET TOTAL"};
		
		for(int i=0;i<4;i++){
			((DefaultTableModel)(theView.getAdminOrdersTable().getModel())).addColumn(columnNames[i]);
		}
		
		theView.getAdminOrdersTable().repaint();
		}	
}
	
	
	

	
	

