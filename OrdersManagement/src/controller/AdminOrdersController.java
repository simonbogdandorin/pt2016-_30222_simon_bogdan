package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

import model.OPDept;
import model.Order;
import model.TheModel;
import view.TheView;

public class AdminOrdersController {
	private TheModel theModel;
	private TheView theView;
	private ActionListener confirmLastOrder;
	
	
	public AdminOrdersController(TheModel theModel, TheView theView){
		this.theModel=theModel;
		this.theView=theView;
	}
	
	public void init(){
		
		confirmLastOrder=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				confirmOrder();
			}
			};;
		theView.getBtnAdminOrderConfirm().addActionListener(confirmLastOrder);

	}
	

	public void initializeOrdersTable(OPDept opdept){
		DefaultTableModel model= (DefaultTableModel) theView.getAdminOrdersTable().getModel();
			model.setRowCount(0);
			for (Map.Entry<Long,Order> entry : opdept.getOrders().entrySet()){
				model.addRow(new Object[] { entry.getValue().getTime(),
		    								entry.getValue().getCustomer().getUsername(),
		    								entry.getValue().simplifiedToString(),
		    								entry.getValue().getTotalPrice()
		    								});
			}
	}
	
	public void confirmOrder(){
		if (theModel.getOpdept().commitOrder())
			initializeOrdersTable(theModel.getOpdept());
		else
			theView.displayMessageDialog("Nu exista comenzi de efectuat!");
	}
	
}
