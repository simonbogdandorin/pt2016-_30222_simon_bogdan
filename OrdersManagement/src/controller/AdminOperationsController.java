package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

import model.Customer;
import model.OPDept;
import model.Order;
import model.Product;
import model.TheModel;
import model.User;
import model.UsersManager;
import model.Warehouse;
import view.TheView;

public class AdminOperationsController {
	private TheView theView;
	private TheModel theModel;
	private ActionListener customersActionListener;
	private ActionListener productsActionListener;
	private ActionListener ordersActionListener;
	
	public AdminOperationsController(TheModel theModel,TheView view){
		this.theView=view;
		this.theModel=theModel;
	}
	
	public void init(){
		ordersActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				theView.getCardLayout().show(theView.getFrame().getContentPane(), "adminOrdersPanel");
				initializeOrdersTable(theModel.getOpdept());
			}
			};
		theView.getBtnAdminOrders().addActionListener(ordersActionListener);
		
		customersActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				initializeCustomersTable(theModel.getUsersManager());
				theView.getAdminCustomerUNTextField().setText("");
				theView.getAdminCustomerPassTextField().setText("");
				theView.getAdminCustomerNameTextField().setText("");
				theView.getAdminCustomerAdrTextField().setText("");
				theView.getAdminCustomerTelTextField().setText("");
				theView.getCardLayout().show(theView.getFrame().getContentPane(), "adminCustomersPanel");	
			}
			};
		theView.getBtnAdminCustomers().addActionListener(customersActionListener);
		
		productsActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				theView.getAdminProdCantTextField().setText("");
				theView.getAdminProdNameTextField().setText("");
				theView.getAdminProdPriceTextField().setText("");

				theView.getCardLayout().show(theView.getFrame().getContentPane(), "adminProductsPanel");
				initializeProductsTable(theModel.getOpdept().getWarehouse());
				initFilterComboBox();
			}
			};
		theView.getBtnAdminProducts().addActionListener(productsActionListener);
		
		
	}
	
	public void initializeOrdersTable(OPDept opdept){
		DefaultTableModel model= (DefaultTableModel) theView.getAdminOrdersTable().getModel();
		model.setRowCount(0);
		if (theModel.getOpdept().getOrders().isEmpty()){
			model.setRowCount(0);
		}
		else
			for (Map.Entry<Long,Order> entry : opdept.getOrders().entrySet()){
				model.addRow(new Object[] { entry.getValue().getTime(),
		    								entry.getValue().getCustomer().getUsername(),
		    								entry.getValue().simplifiedToString(),
		    								entry.getValue().getTotalPrice()
		    								});
			}
	}
	
	public void initializeCustomersTable(UsersManager um){
		DefaultTableModel model= (DefaultTableModel) theView.getAdminCustomersTable().getModel();
		model.setRowCount(0);
		
		for (User entry : um.getUsers()){
			if(entry instanceof Customer)
				model.addRow(new Object[] { entry.getUsername(),
		    								entry.getPass(),
		    								((Customer) entry).getName(),
		    								((Customer) entry).getTelephone(),
		    								((Customer) entry).getAdress()
		    								});
		}
	}
	
	public void initializeProductsTable(Warehouse warehouse){
		DefaultTableModel model= (DefaultTableModel) theView.getAdminProductsTable().getModel();
		model.setRowCount(0);
		for (Map.Entry<String,Product> entry : warehouse.getProducts().entrySet()){
		    model.addRow(new Object[] { entry.getValue().getProdCategory().toString(),
		    							entry.getValue().getProdName(),
		    							entry.getValue().getProdPrice(),
		    							entry.getValue().getProdCant()
		    							});
		}
	}
	
	public void initFilterComboBox(){
		theView.getAdminProdCatComboBox().addItem(Product.Category.ALIMENTE);
		theView.getAdminProdCatComboBox().addItem(Product.Category.ELECTROCASNICE);
		theView.getAdminProdCatComboBox().addItem(Product.Category.BAUTURI);
		theView.getAdminProdCatComboBox().addItem(Product.Category.IT);
		theView.getAdminProdCatComboBox().addItem(Product.Category.PAPETARIE);
		theView.getAdminProdCatComboBox().addItem(Product.Category.COSMETICE);
		theView.getAdminProdCatComboBox().addItem(Product.Category.IMBRACAMINTE);
		theView.getAdminProdCatComboBox().addItem(Product.Category.MOBILIER);
		theView.getAdminProdCatComboBox().addItem(Product.Category.SPORT);
	}
	
}

