package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

import model.Customer;
import model.Order;
import model.Product;
import model.TheModel;
import view.TheView;

public class CustomerOperationsController {
	private TheView theView;
	private TheModel theModel;
	private ActionListener adaugaActionListener;
	private ActionListener trimiteActionListener;
	private ActionListener filtreazaActionListener;
	
	
	
	//CONSTRUCTORS
	public CustomerOperationsController(TheModel model,TheView view){
		this.theModel=model;
		this.theView=view;
	}
	
	public void init(){
		
		//ADAUGA LA COMANDA
		adaugaActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				addToTheOrder();	
			}
			};
		theView.getBtnAdaugaLaComanda().addActionListener(adaugaActionListener);
		
		//TRIMITE COMANDA
		trimiteActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				sendTheOrder();
			}
			};
		theView.getBtnTrimiteComanda().addActionListener(trimiteActionListener);
		
		//FILTREAZA PRODUSE
		filtreazaActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				filterProducts();
			}
		};
		theView.getBtnCustomerProductsFilter().addActionListener(filtreazaActionListener);
	
	}
	
	public void addToTheOrder(){
		int selectedRow = theView.getCustomerProductsTable().getSelectedRow();
		selectedRow = theView.getCustomerProductsTable().convertRowIndexToModel(selectedRow);
		if(selectedRow==-1){
			theView.displayMessageDialog("Niciun rand selectat !");
			return ;
		}
		DefaultTableModel model = (DefaultTableModel)theView.getCustomerProductsTable().getModel();
		int nrOfColumns=model.getColumnCount();
		Object[] fields=new Object[nrOfColumns];
		for(int i=0;i<nrOfColumns;i++){
			fields[i]=model.getValueAt(selectedRow,i);
		}
		model= (DefaultTableModel) theView.getCustomerCrtOrdersTable().getModel();
		model.addRow(fields);
		theView.getCustomerCrtOrdersTable().repaint();
	}
	
	public void sendTheOrder(){
		List<Product> products=new ArrayList<Product>();
		DefaultTableModel model = (DefaultTableModel)theView.getCustomerCrtOrdersTable().getModel();
		int nrOfRows=model.getRowCount();
		int nrOfColumns=model.getColumnCount();
		if(nrOfRows==0){
			theView.displayMessageDialog("Niciun produs adaugat la comanda! ");
			return ;
		}
		Object[] fields=new Object[nrOfColumns];
		for(int i=0;i<nrOfRows;i++){
			for(int j=0;j<nrOfColumns;j++){
				fields[j]=model.getValueAt(i, j);
			}
			try{
				Product prod=new Product(fields);
				if (prod.getProdCant()<0){
					theView.displayMessageDialog("Ai introdus o cantitate negativa!");
					return;
				}
				products.add(prod);
			}catch(NumberFormatException e){
					theView.displayMessageDialog("Ai introdus o cantitate invalida!");
					return;
			}
		}
		Order order=new Order(products,(Customer)NavController.crtUser);
		if (!theModel.getOpdept().addOrder(order)){
			theView.displayMessageDialog("UNDERSTOCK!");
			return ;
		}
		model.setRowCount(0);
	}
	
	
	
	public void initFilterComboBox(){
		theView.getCustomerFilterComboBox().addItem(Product.Category.ALIMENTE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.ELECTROCASNICE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.BAUTURI);
		theView.getCustomerFilterComboBox().addItem(Product.Category.IT);
		theView.getCustomerFilterComboBox().addItem(Product.Category.PAPETARIE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.COSMETICE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.IMBRACAMINTE);
		theView.getCustomerFilterComboBox().addItem(Product.Category.MOBILIER);
		theView.getCustomerFilterComboBox().addItem(Product.Category.SPORT);
	}
	
	public void filterProducts(){
		
		DefaultTableModel model= (DefaultTableModel) theView.getCustomerProductsTable().getModel();
		model.setRowCount(0);
		for (Map.Entry<String,Product> entry : theModel.getOpdept().getWarehouse().getProducts().entrySet()){
			if(entry.getValue().getProdCategory()== theView.getCustomerFilterComboBox().getSelectedItem())
		    	model.addRow(new Object[] { entry.getValue().getProdCategory().toString(),
		    								entry.getValue().getProdName(),
		    								entry.getValue().getProdPrice(),
		    								entry.getValue().getProdCant()
		    							});
		}	
	}
}
