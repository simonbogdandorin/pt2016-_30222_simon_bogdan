package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Admin;
import model.Customer;
import model.OPDept;
import model.Order;
import model.Product;
import model.TheModel;
import model.User;
import model.UsersManager;
import model.Warehouse;
import model.Product.Category;

public class JUnitTesting {

	@Test
	public void test() {
		OPDept opdept = new OPDept();
		UsersManager usersManager = new UsersManager();
		Warehouse warehouse= new Warehouse();
		List<User> users=new ArrayList<User>();
		List<Product> orderedProds = new ArrayList<Product>();
		List<Product> orderedProds2 = new ArrayList<Product>();
		
		User admin1=new Admin("simonbogdandorin",
				"simonbogdandorin");
		User customer1=new Customer("Popescu Ion","popion","passpopion",
									"Com. Recea, str.Principala, nr. 120A","0740085954");
		User customer2=new Customer("Ionescu Alex","ionescualex","ionescualex",
				"Brasov,Str.Libertatii, nr. 23","0745632334");
		users.add(admin1);
		users.add(customer1);
		users.add(customer2);
		usersManager.setUsers(users);
		
		Product prod1= new Product("laptop",Category.IT , 2500.0, 3);
		Product prod2= new Product("masina de spalat",Category.ELECTROCASNICE , 3210.50, 2);
		Product prod3= new Product("televizor",Category.ELECTROCASNICE , 1500.0, 19);
		Product prod4= new Product("rosii",Category.ALIMENTE , 2.99, 23);
		Product prod5= new Product("castraveti",Category.ALIMENTE , 1.50, 1);
		warehouse.createProd(prod1);
		warehouse.createProd(prod2);
		warehouse.createProd(prod3);
		warehouse.createProd(prod4);
		warehouse.createProd(prod5);
		opdept.setWarehouse(warehouse);	
		
		Product orderedProd1=new Product("laptop",Category.IT , 2500.0, 50);
		Product orderedProd2= new Product("rosii",Category.ALIMENTE , 2.99, 12);
		Product orderedProd3= new Product("castraveti",Category.ALIMENTE , 2.99, 1);
		
		orderedProds.add(orderedProd1);
		orderedProds.add(orderedProd2);
		orderedProds2.add(orderedProd3);
		
		Order ord= new Order(orderedProds,(Customer)customer1);
		Order ord2=new Order(orderedProds2,(Customer)customer2);
		
		boolean succeded;
		assertEquals(false,opdept.addOrder(ord));
		
		
		
		
		
	}

}
