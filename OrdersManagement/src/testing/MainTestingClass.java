package testing;

import java.util.ArrayList;


import model.*;
import model.Product.Category;


public class MainTestingClass {

	public static void main(String[] args) {
		
		Warehouse warehouse= new Warehouse();
		/*
		User customer1=new Customer("Simon Bogdan-Dorin","simonbogdandorin",
										"pass_simonbogdandorin","Reghin, str.Beng, nr.7");
		
		*/
		Product prod1= new Product("leptop",Category.IT , 2500.0, 3);
		Product prod2= new Product("masina de spalat",Category.ELECTROCASNICE , 3210.50, 2);
		Product prod3= new Product("televizor",Category.ELECTROCASNICE , 1500.0, 19);
		Product prod4= new Product("rosii",Category.ALIMENTE , 2.99, 23);
		Product prod5= new Product("castraveti",Category.ALIMENTE , 1.50, 1);
		Product prod6= new Product("caciula",Category.IMBRACAMINTE , 9.99, 10);
		Product prod7= new Product("telefon",Category.IT , 820.00, 0);
		Product prod8= new Product("parchet",Category.CONSTRUCTII , 40.00, 21);
		Product prod9= new Product("parfum",Category.COSMETICE , 300.0, 0);
		Product prod10= new Product("crema",Category.COSMETICE , 60.0, 32);
		Product prod11= new Product("dulap",Category.MOBILIER , 2500.0, 21);
		Product prod12= new Product("birou",Category.MOBILIER , 435.20, 32);
		Product prod13= new Product("pix",Category.PAPETARIE, 2.50, 3);
		Product prod14= new Product("hartie",Category.PAPETARIE , 15.0, 3);
		Product prod15= new Product("pasta de dinti",Category.COSMETICE , 11.0, 55);
		Product prod16= new Product("vodka",Category.BAUTURI , 39.99, 0);
		Product prod17= new Product("vin",Category.BAUTURI , 45.99, 0);
		Product prod18= new Product("bere",Category.BAUTURI , 9.0, 4);
		Product prod19= new Product("caramida",Category.CONSTRUCTII , 0.1, 65);
		Product prod20= new Product("geaca",Category.IMBRACAMINTE , 200.0, 32);
		Product prod21= new Product("pantofi",Category.IMBRACAMINTE , 140.0, 76);
		Product prod22= new Product("pantaloni",Category.IMBRACAMINTE , 60.0, 2);
		Product prod23= new Product("minge",Category.SPORT , 10.0, 23);
		Product prod24= new Product("palinca",Category.BAUTURI , 8.0, 12);
		Product prod25= new Product("parchet",Category.CONSTRUCTII , 40.00, 5);
		
		warehouse.createProd(prod1);
		warehouse.createProd(prod2);
		warehouse.createProd(prod3);
		warehouse.createProd(prod4);
		warehouse.createProd(prod5);
		warehouse.createProd(prod6);
		warehouse.createProd(prod7);
		warehouse.createProd(prod8);
		warehouse.createProd(prod9);
		warehouse.createProd(prod10);
		warehouse.createProd(prod11);
		warehouse.createProd(prod12);
		warehouse.createProd(prod13);
		warehouse.createProd(prod14);
		warehouse.createProd(prod15);
		warehouse.createProd(prod16);
		warehouse.createProd(prod17);
		warehouse.createProd(prod18);
		warehouse.createProd(prod19);
		warehouse.createProd(prod20);
		warehouse.createProd(prod21);
		warehouse.createProd(prod22);
		warehouse.createProd(prod23);
		warehouse.createProd(prod24);
		
		ArrayList<Product> orderedProducts1=new ArrayList<Product>();
		orderedProducts1.add(prod25);
		
		//Order order1=new Order(orderedProducts1,(Customer)customer1);
		OPDept opdept=new OPDept(warehouse);
		
		//opdept.addOrder(order1);
		
		opdept.commitOrder();
	
		/*
		succes=warehouse.createProd(prod25);
		if(succes){
			System.out.println("Adaugat cu succes!");
		}
		else {
			System.out.println("Stocul este plin!");
		}
		*/
		//ITERARE
		/*
		System.out.println(warehouse.toString());
		
		for (Map.Entry<String,Product> entry : warehouse.getProducts().entrySet()){
		    System.out.println(entry.getValue().toString());
		}
		*/
		
	}

}
