package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import model.Calculator;
import model.MonomInt;
import model.Monomial;
import model.Polynomial;

public class JUnitTestingCase {

	@Test
	public void test() {
		Calculator calculator = new Calculator();
		Polynomial polynom1;
		Polynomial polynom2;
		ArrayList<Monomial> monomList1= new ArrayList<Monomial>();
		ArrayList<Monomial> monomList2= new ArrayList<Monomial>();
		
		MonomInt monom11 = new MonomInt(-1,0);
		MonomInt monom12 = new MonomInt(2,1);
		MonomInt monom13 = new MonomInt(-3,2);
		monomList1.add(monom11);
		monomList1.add(monom12);
		monomList1.add(monom13);
		
		MonomInt monom21 = new MonomInt(1,0);
		MonomInt monom22 = new MonomInt(2,1);
		MonomInt monom23 = new MonomInt(4,2);
		monomList2.add(monom21);
		monomList2.add(monom22);
		monomList2.add(monom23);
		
		polynom1= new Polynomial(monomList1);
		polynom2= new Polynomial(monomList2);
		Polynomial polynom3= calculator.addPolynomials(polynom1, polynom2);
		
		for(int i=0;i<=polynom3.getDegree();i++){
			assertEquals((polynom1.getMonomList().get(i).getCoeff().intValue()+
					polynom2.getMonomList().get(i).getCoeff().intValue()), polynom3.getMonomList().get(i).getCoeff().intValue());
		
		}
		
		
		
		
		
	}

}
