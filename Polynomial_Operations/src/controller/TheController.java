package controller;


import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import model.*;
import view.*;
import java.awt.event.ActionEvent;

/**
 * Clasa TheController realizeaza legatura dintre interfata si model. Ea construieste modelul , adica polinoamele pe baza datelor de intrare
 * , iar mai apoi rezultatul de afisat pe baza polinoamelor rezultat din model. Totodata, aici se semnaleaza 
 * apelul utilizatorului pentru executia unei operatii, precum si operatia dorita.
 * @author Bogdam
 *
 */

public class TheController {
	TheView theView ;
	TheModel theModel;

	ActionListener actionListener;
	ItemListener itemListener;
	
	//CONSTRUCTORS

	public TheController(TheView theView, TheModel theModel){
		this.theView=theView;
		this.theModel=theModel;

	}
	
	/**
	 * Metoda control este apelata in clasa main. Ea semnaleaza apasarea butonului de Calculeaza
	 * de cate utilizator si da mai departe controlul interpretorului de date de intrare.
	 */
	public void control(){

		actionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				
				interpretInput();
			}
		};;
		theView.getBtnCalculeaza().addActionListener(actionListener);
	}
	
	/*
	 * In cadrul acestei metode se stabileste oepratia dorita de catre utilizator, apelandu-se mai departe metodele ce costruiesc
	 * polinoamele, dupa ce textul a fost luat din TextField-uri, metodele de calcul si de afisare a rezultatului.
	 * In cadrul metodei se prind exceptiile legate de introducerea invalida a datelor de intrare.
	 */
	public void interpretInput(){
		String operatie =(String) theView.getComboBoxOperatii().getSelectedItem();
		
		switch(operatie){
			case "Adunare": 
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				theModel.setPolynom2(constructPolynomial(theView.getTextFieldPol2().getText()));
				}catch(NumberFormatException e){
					theView.displayMessageDialog("Invalid input!");
					return;
				}
				 catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				
				if(theModel.getPolynom1()!=null && theModel.getPolynom2()!=null){
				theModel.setPolynom3(theModel.getCalculator().addPolynomials(theModel.getPolynom1(), theModel.getPolynom2()));
				theView.getTextFieldRez1().setText(constructOutput(theModel.getPolynom3()));
				}
				else return;
			
				break;
			case "Scadere": 
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				theModel.setPolynom2(constructPolynomial(theView.getTextFieldPol2().getText()));
				}catch(NumberFormatException e){
						theView.displayMessageDialog("Invalid input!");
						return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null && theModel.getPolynom2()!=null){
				theModel.setPolynom3(theModel.getCalculator().subPolynomials(theModel.getPolynom1(), theModel.getPolynom2()));
				theView.getTextFieldRez1().setText(constructOutput(theModel.getPolynom3()));
				}
				else return;
				break;
			case "Inmultire": 
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				theModel.setPolynom2(constructPolynomial(theView.getTextFieldPol2().getText()));
				}catch(NumberFormatException e){
						theView.displayMessageDialog("Invalid input!");
						return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null && theModel.getPolynom2()!=null){
				theModel.setPolynom3(theModel.getCalculator().mulPolynomials(theModel.getPolynom1(), theModel.getPolynom2()));
				theView.getTextFieldRez1().setText(constructOutput(theModel.getPolynom3()));
				}
				else return;
				break;
			case "Impartire":
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				theModel.setPolynom2(constructPolynomial(theView.getTextFieldPol2().getText()));
				}catch(NumberFormatException e){
					theView.displayMessageDialog("Invalid input!");
					return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null && theModel.getPolynom2()!=null){
				try{
				theModel.setPolynom3(theModel.getCalculator().divPolynomials(theModel.getPolynom1(), theModel.getPolynom2()));
				}catch(ArithmeticException e){
					theView.displayMessageDialog("Illegal division by zero.");
					return;
				}
				theView.getTextFieldRez1().setText(constructOutput(theModel.getPolynom3()));
				theView.getTextFieldRez2().setText(constructOutput(theModel.getPolynom1()));
				}
				else return;
				break;
			case "Derivare": 
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				}catch(NumberFormatException e){
						theView.displayMessageDialog("Invalid input!");
						return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null ){
				theModel.setPolynom3(theModel.getCalculator().diffPolynomials(theModel.getPolynom1()));
				theView.getTextFieldRez1().setText(constructOutput(theModel.getPolynom3()));
				}
				else return;
				break;
			case "Integrare nedefinita": 
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				}catch(NumberFormatException e){
					theView.displayMessageDialog("Invalid input!");
					return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null){
				theModel.setPolynom3(theModel.getCalculator().indefIntPolynomials(theModel.getPolynom1()));
				theView.getTextFieldRez1().setText(constructOutput(theModel.getPolynom3()));
				}
				else return;
				break;
			case "Integrare definita": 
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				theModel.setBelowLimit(Integer.parseInt(theView.getTextFieldLimInf().getText()));
				theModel.setAboveLimit(Integer.parseInt(theView.getTextFieldLimSup().getText()));
				}catch(NumberFormatException e){
						theView.displayMessageDialog("Invalid input!");
						return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null && theModel.getPolynom2()!=null){
				theView.getTextFieldRez1().setText(Double.toString(theModel.getCalculator().defIntPolynomials(theModel.getPolynom1(),theModel.getBelowLimit(),theModel.getAboveLimit())));
				}
				else return;
				break;
			case "Valoarea intr-un punct":
				try{
				theModel.setPolynom1(constructPolynomial(theView.getTextFieldPol1().getText()));
				theModel.setValue(Integer.parseInt(theView.getTextFieldVal().getText()));
				}catch(NumberFormatException e){
						theView.displayMessageDialog("Invalid input!");
							return;
				}
				catch(ArrayIndexOutOfBoundsException e2){
					 theView.displayMessageDialog("Invalid input!");
						return;
				 }
				if(theModel.getPolynom1()!=null && theModel.getPolynom2()!=null){
				theView.getTextFieldRez1().setText(Double.toString(theModel.getCalculator().valueOfPolynomialFunc(theModel.getPolynom1(), theModel.getValue())));
				}
				else return;
				break;
			default: System.out.println("NUUUU"); //
					 break;
		}
	
	}
	
	//INPUT-DATA and DATA-OUTPUT converters
	/**
	 * Metoda construieste polinoamele pe baza string-ului furnizat de utilizator prin interfata.
	 * Mecanismul de construire este unul simplu si consta in impartirea stringului intr-un sir de stringuri reprezentand monoame(split dupa + si -)
	 * Pe baza determinarii pozitiei lui x se obtin coeficientii si puterile, converite la intregi construindu-se astfel monoamele
	 * Monoamele vor fi adunate intr-o lista de monoame data mai apoi ca si argument constructorului ce construieste polinomul
	 * caracteristic intrarii.
	 * @param input String-ul furnizat de utilizator.
	 * @return un obiect de tip Polynomial, construit pe baza input-ului.
	 */
	public Polynomial constructPolynomial(String input){
		Polynomial polynom;
		List<Monomial> monomList= new ArrayList<Monomial>();
		input = input.replaceAll("\\s", ""); //Pentru simplitate, elimin spatiile albe din input
		String[] stringMonoms= input.split("\\-|\\+"); //Impart input-ul in monoame (substring-urile separate de + si -)
		
		for(int i=0;i<stringMonoms.length;i++){
			
			if (stringMonoms[i].equals("")) {
				continue;
				}
			Integer xPos;
			if(((stringMonoms[i].indexOf('x'))==-1) && (stringMonoms[i].indexOf('X')==-1)){
				Integer coeff= Integer.parseInt(stringMonoms[i].substring(0,1));
				char semn=input.charAt(input.lastIndexOf(stringMonoms[i])-1);
				if (semn=='-') coeff=-coeff;
				Monomial monomResult=new MonomInt(coeff,0);
				monomList.add(monomResult);
			}
			else{
			xPos= (stringMonoms[i].indexOf('x'))>(stringMonoms[i].indexOf('X')) ? (stringMonoms[i].indexOf('x')):(stringMonoms[i].indexOf('X'));
			Integer coeff= Integer.parseInt(stringMonoms[i].substring(0,xPos-1));
			char semn=input.charAt(input.lastIndexOf(stringMonoms[i])-1);
			if (semn=='-') coeff=-coeff;
			Integer pow= Integer.parseInt(stringMonoms[i].substring(xPos+2,stringMonoms[i].length()));
			Monomial monomResult=new MonomInt(coeff,pow);
			monomList.add(monomResult);
		}
		}

		polynom = new Polynomial(monomList);
		
		
		for(int i= polynom.getDegree()-1;i>=0;i--){
			if(!polynom.existsMonom(i)){
				Monomial monomResult=new MonomInt(0,i);
				monomList.add(monomResult);
			}
		}
		polynom=new Polynomial(monomList);

		return polynom;
		
	}
	/**
	 * Metoda realizeaza o conversie polinom- string, pentru afisarea polinoamelor rezultat. Mecanismul este unul extrem de simplu
	 * este parcurs sirul de monoame al polinomului dat ca si argument, iar sirul de output este construit succesiv
	 * se tine cont de semnul coeficientilor si (se adauga + sau -), se adauga coeficientul convertit la caracter,
	 * se concateneaza cu sutringul '*x^' , iar mai apoi se adauga puterea, evident, converita la randul ei la caracter.
	 * @param polynom Obiectul de tip Polynomial de afisat pe ecran ca si rezultat al operatiei anterioare.
	 * @return Un String reprezentand forma valida pentru afisare a polinomului rezultat.
	 */
	public String constructOutput(Polynomial polynom){
		String output="";
		
		for(int i=polynom.getDegree();i>=0;i--){
			
			if(polynom.getMonomList().get(i).getCoeff().doubleValue()!=0)
			{	if(polynom.getMonomList().get(i).getCoeff().doubleValue()>0)	output+='+';
														
				if(polynom.getMonomList().get(i).getDegree()!=0)
						output+=polynom.getMonomList().get(i).getCoeff()+"*X^"+polynom.getMonomList().get(i).getDegree();
				else	
					output+=polynom.getMonomList().get(i).getCoeff();
			}
		
		}
		
		return output;
	}
	
	
	
	//GETTERS & SETTERS
	public TheView getUserInterface(){
		return this.theView;
	}
	public void theView(TheModel theModel){
		this.theModel=theModel;
	}
	public TheModel getModel(){
		return this.theModel;
	}
	public void setModel(TheModel theModel){
		this.theModel=theModel;
	}
	

}
