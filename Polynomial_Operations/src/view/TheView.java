package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 * Clasa care se ocupa cu interfata grafica.
 * @author Bogdam
 *
 */
public class TheView {
	
	
	
	private JFrame frmPolynomialOperations;
	private JTextField textFieldPol1;
	private JTextField textFieldPol2;
	private JTextField textFieldRez1;
	private JTextField textFieldRez2;
	private JTextField textFieldLimInf;
	private JTextField textFieldLimSup;
	private JTextField textFieldVal;
	private JButton btnCalculeaza;
	private JComboBox comboBoxOperatii;
	private JLabel lblPol1;
	private JLabel lblPol2;
	private JLabel lblRez1;
	private JLabel lblRez2;
	private JLabel lblOperatie;
	private JLabel lblLimInf;
	private JLabel lblLimSup;
	private JLabel lblValue;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TheView window = new TheView();
					window.frmPolynomialOperations.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TheView() {
		frmPolynomialOperations = new JFrame();
		frmPolynomialOperations.getContentPane().setEnabled(false);
		textFieldPol2 = new JTextField();
		textFieldPol1 = new JTextField();
		textFieldRez1 = new JTextField();
		textFieldRez2 = new JTextField();
		textFieldLimInf = new JTextField();
		textFieldLimSup = new JTextField();
		textFieldVal = new JTextField();
		btnCalculeaza = new JButton("Calculeaz\u0103 !");
		comboBoxOperatii = new JComboBox();
		lblPol1= new JLabel("Pol. 1:");
		lblPol2 = new JLabel("Pol. 2:");
		lblRez1 = new JLabel("Rezultat 1:");
		lblRez2 = new JLabel("Rezultat 2:");
		lblOperatie = new JLabel("Operatie:");
		lblLimInf = new JLabel("Limita inf:");
		lblLimSup = new JLabel("Limita sup:");
		lblValue = new JLabel("\u00CEn punctul:");
		
		initialize();
	}

	/**
	 * Initializeaza continutul frame-ului
	 */
	private void initialize() {
		this.frmPolynomialOperations.setVisible(true);
		frmPolynomialOperations.setResizable(false);
		frmPolynomialOperations.getContentPane().setFont(new Font("Microsoft YaHei UI", Font.BOLD, 13));
		frmPolynomialOperations.setTitle("Polynomial operations");
		frmPolynomialOperations.getContentPane().setBackground(new Color(153, 204, 102));
		
		textFieldPol1.setToolTipText("an*x^n + ... + a1*x+a0");
		textFieldPol1.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 11));
		textFieldPol1.setColumns(10);
		
		textFieldPol2.setToolTipText("an*x^n + ... + a1*x+a0");
		textFieldPol2.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 11));
		textFieldPol2.setColumns(10);
		
		lblPol1.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		
		lblPol2.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		
		comboBoxOperatii.setFont(new Font("Yu Gothic UI Light", Font.BOLD, 12));
		comboBoxOperatii.setToolTipText("Alege operatia");
		comboBoxOperatii.setModel(new DefaultComboBoxModel(new String[] {"Adunare", "Scadere", "Inmultire", "Impartire", "Derivare", "Integrare nedefinita", "Integrare definita", "Valoarea intr-un punct"}));
		
		ItemListener itemListener = new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				setVisibility();
			}
			
		};
		
		comboBoxOperatii.addItemListener(itemListener);
		
		lblOperatie.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		
		textFieldRez1.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 11));
		textFieldRez1.setColumns(10);
		
		textFieldRez2.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 11));
		textFieldRez2.setColumns(10);
		textFieldRez2.setVisible(false);
		
		lblRez1.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 12));
		
		lblRez2.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 12));
		lblRez2.setVisible(false);
		
		textFieldLimInf.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 11));
		textFieldLimInf.setColumns(10);
		textFieldLimInf.setVisible(false);
		
		lblLimInf.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		lblLimInf.setVisible(false);
		
		textFieldLimSup.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 11));
		textFieldLimSup.setColumns(10);
		textFieldLimSup.setVisible(false);
		
		lblLimSup.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		lblLimSup.setVisible(false);
		
		textFieldVal.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 11));
		textFieldVal.setColumns(10);
		textFieldVal.setVisible(false);
		
		lblValue.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		lblValue.setVisible(false);
		
		btnCalculeaza.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		btnCalculeaza.setHorizontalAlignment(SwingConstants.RIGHT);
		GroupLayout groupLayout = new GroupLayout(frmPolynomialOperations.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblPol1)
						.addComponent(lblPol2)
						.addComponent(lblRez1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE)
						.addComponent(lblRez2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblOperatie, GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE))
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(comboBoxOperatii, 0, 337, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblLimInf, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textFieldLimInf, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
							.addGap(79)
							.addComponent(lblLimSup)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textFieldLimSup, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
							.addGap(73))
						.addComponent(textFieldRez1, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
						.addComponent(textFieldRez2, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
						.addComponent(textFieldPol2, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
						.addComponent(textFieldPol1, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
					.addGap(37))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(146)
					.addComponent(lblValue)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textFieldVal, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(204, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(178, Short.MAX_VALUE)
					.addComponent(btnCalculeaza)
					.addGap(169))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldPol1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPol1, GroupLayout.PREFERRED_SIZE, 18, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldPol2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPol2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLimInf, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblLimSup)
						.addComponent(textFieldLimSup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldLimInf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldVal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblValue))
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBoxOperatii, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblOperatie))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCalculeaza, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldRez1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblRez1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldRez2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblRez2, GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
					.addGap(21))
		);
		frmPolynomialOperations.getContentPane().setLayout(groupLayout);
		frmPolynomialOperations.setBounds(100, 100, 450, 300);
		frmPolynomialOperations.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * Metoda care seteaza care componente (etichete, campuri de text etc.) sunt vizibile, 
	 * in functie de operatia aleasa si deci datele care trebuiesc introduse de utilizator.
	 */
	public void setVisibility(){
		String operatie = (String)comboBoxOperatii.getSelectedItem();
		switch(operatie){
		case "Adunare": 
			lblPol2.setVisible(true);
			textFieldPol2.setVisible(true);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);
			break;
		case "Scadere": 
			lblPol2.setVisible(true);
			textFieldPol2.setVisible(true);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);
			break;
		case "Inmultire": 
			lblPol2.setVisible(true);
			textFieldPol2.setVisible(true);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);
			break;
		case "Impartire": 
			lblPol2.setVisible(true);
			textFieldPol2.setVisible(true);
			lblRez2.setVisible(true);
			textFieldRez2.setVisible(true);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);
			break;
		case "Derivare":
			lblPol2.setVisible(false);
			textFieldPol2.setVisible(false);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);
			break;
		case "Integrare nedefinita": 
			lblPol2.setVisible(false);
			textFieldPol2.setVisible(false);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);	
			break;
		case "Integrare definita": 
			lblPol2.setVisible(false);
			textFieldPol2.setVisible(false);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(true);
			textFieldLimInf.setVisible(true);
			lblLimSup.setVisible(true);
			textFieldLimSup.setVisible(true);
			lblValue.setVisible(false);
			textFieldVal.setVisible(false);
			break;
		case "Valoarea intr-un punct":
			lblPol2.setVisible(false);
			textFieldPol2.setVisible(false);
			lblRez2.setVisible(false);
			textFieldRez2.setVisible(false);
			lblLimInf.setVisible(false);
			textFieldLimInf.setVisible(false);
			lblLimSup.setVisible(false);
			textFieldLimSup.setVisible(false);
			lblValue.setVisible(true);
			textFieldVal.setVisible(true);
		default: 
			break;
	}
	}
	
	/**
	 * Se afiseaza un mesaj, de regula atunci cand ceva nu a functionat corect ( ex:
	 * date introduse sunt invalide, s-a incercat impartirea la polinomul 0 etc.)
	 * @param message Mesajul de afisat impreuna cu dialog-ul aparut pe ecran.
	 */
	public void displayMessageDialog(String message){
	JOptionPane.showMessageDialog(frmPolynomialOperations, message);
	}
	
	//GETTERS & SETTERS
	public JFrame getFrmPolynomialOperations() {
		return frmPolynomialOperations;
	}

	public void setFrmPolynomialOperations(JFrame frmPolynomialOperations) {
		this.frmPolynomialOperations = frmPolynomialOperations;
	}

	public JTextField getTextFieldPol1() {
		return textFieldPol1;
	}

	public void setTextFieldPol1(JTextField textFieldPol1) {
		this.textFieldPol1 = textFieldPol1;
	}

	public JTextField getTextFieldPol2() {
		return textFieldPol2;
	}

	public void setTextFieldPol2(JTextField textFieldPol2) {
		this.textFieldPol2 = textFieldPol2;
	}

	public JTextField getTextFieldRez1() {
		return textFieldRez1;
	}

	public void setTextFieldRez1(JTextField textFieldRez1) {
		this.textFieldRez1 = textFieldRez1;
	}

	public JTextField getTextFieldRez2() {
		return textFieldRez2;
	}

	public void setTextFieldRez2(JTextField textFieldRez2) {
		this.textFieldRez2 = textFieldRez2;
	}

	public JTextField getTextFieldLimInf() {
		return textFieldLimInf;
	}

	public void setTextFieldLimInf(JTextField textFieldLimInf) {
		this.textFieldLimInf = textFieldLimInf;
	}

	public JTextField getTextFieldLimSup() {
		return textFieldLimSup;
	}

	public void setTextFieldLimSup(JTextField textFieldLimSup) {
		this.textFieldLimSup = textFieldLimSup;
	}

	public JTextField getTextFieldVal() {
		return textFieldVal;
	}

	public void setTextFieldVal(JTextField textFieldVal) {
		this.textFieldVal = textFieldVal;
	}
	
	public JButton getBtnCalculeaza(){
		return this.btnCalculeaza;
	}
	
	public void setBtnCalculeaza(JButton btnCalculeaza){
		this.btnCalculeaza=btnCalculeaza;
	}

	public JComboBox getComboBoxOperatii() {
		return comboBoxOperatii;
	}

	public void setComboBoxOperatii(JComboBox comboBoxOperatii) {
		this.comboBoxOperatii = comboBoxOperatii;
	}

	public JLabel getLblPol1() {
		return lblPol1;
	}

	public void setLblPol1(JLabel lblPol1) {
		this.lblPol1 = lblPol1;
	}

	public JLabel getLblPol2() {
		return lblPol2;
	}

	public void setLblPol2(JLabel lblPol2) {
		this.lblPol2 = lblPol2;
	}

	public JLabel getLblRez1() {
		return lblRez1;
	}

	public void setLblRez1(JLabel lblRez1) {
		this.lblRez1 = lblRez1;
	}

	public JLabel getLblRez2() {
		return lblRez2;
	}

	public void setLblRez2(JLabel lblRez2) {
		this.lblRez2 = lblRez2;
	}

	public JLabel getLblOperatie() {
		return lblOperatie;
	}

	public void setLblOperatie(JLabel lblOperatie) {
		this.lblOperatie = lblOperatie;
	}

	public JLabel getLblLimInf() {
		return lblLimInf;
	}

	public void setLblLimInf(JLabel lblLimInf) {
		this.lblLimInf = lblLimInf;
	}

	public JLabel getLblLimSup() {
		return lblLimSup;
	}

	public void setLblLimSup(JLabel lblLimSup) {
		this.lblLimSup = lblLimSup;
	}

	public JLabel getLblValue() {
		return lblValue;
	}

	public void setLblValue(JLabel lblValue) {
		this.lblValue = lblValue;
	}
	
	
}
