package model;


/**
 * Clasa insumeaza toate componentele modelului necesar rezolvarii cetintei:
 * 4 polinoame pentru operanzi, precum si un calculator pentru rezolvarea operatiilor.
 * @author Bogdam
 *
 */
public class TheModel {
	private Calculator calculator = new Calculator();
	private Polynomial polynom1;
	private Polynomial polynom2;
	private Polynomial polynom3;
	private Polynomial polynom4;
	private Integer belowLimit, aboveLimit;
	private Integer value;
	
	//CONSTRUCTORS
	public TheModel(){
		
		calculator= new Calculator();
		polynom1 = new Polynomial();
		polynom2 = new Polynomial();
		polynom3 = new Polynomial();
		polynom4 = new Polynomial();
		belowLimit=0;
		aboveLimit=0;
		value=0;
	}
	
	//GETTERS & SETTERS
	public Calculator getCalculator() {
		return calculator;
	}

	public void setCalculator(Calculator calculator) {
		this.calculator = calculator;
	}

	public Polynomial getPolynom1() {
		return polynom1;
	}

	public void setPolynom1(Polynomial polynom1) {
		this.polynom1 = polynom1;
	}

	public Polynomial getPolynom2() {
		return polynom2;
	}

	public void setPolynom2(Polynomial polynom2) {
		this.polynom2 = polynom2;
	}

	public Polynomial getPolynom3() {
		return polynom3;
	}

	public void setPolynom3(Polynomial polynom3) {
		this.polynom3 = polynom3;
	}

	public Polynomial getPolynom4() {
		return polynom4;
	}

	public void setPolynom4(Polynomial polynom4) {
		this.polynom4 = polynom4;
	}

	public Integer getBelowLimit() {
		return belowLimit;
	}

	public void setBelowLimit(Integer belowLimit) {
		this.belowLimit = belowLimit;
	}

	public Integer getAboveLimit() {
		return aboveLimit;
	}

	public void setAboveLimit(Integer aboveLimit) {
		this.aboveLimit = aboveLimit;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
