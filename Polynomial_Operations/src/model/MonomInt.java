package model;

/**
 * Clasa modeleaza un monom cu coeficienti intregi. Ea primeste de la clasa Monomial metodele si gradul si implementeaza metodata
 * abstract getCoeff si setCoeff pentru setarea si determinarea coeficientului.
 * @author Bogdam
 *
 */
public class MonomInt extends Monomial {
	Integer coeff;
	
	//CONSTRUCTORS
	public MonomInt(Integer coeff, Integer degree){
		super(degree);
		this.coeff=coeff;
	}
	
	//GETTERS AND SETTERS
	
	public Integer getCoeff(){
		return this.coeff;
	}
	public void setCoeff(Number coeff){
		this.coeff=  coeff.intValue();
	}
	
}
