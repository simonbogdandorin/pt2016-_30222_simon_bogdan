package model;

import java.util.Collections;
import java.util.ArrayList;



/**
 * Clasa care contine implementarea metodelor ce opereaza pe polinoame.
 * @author Bogdam
 *
 */

public class Calculator implements Operations {
	
	//CONSTRUCTORS
	public Calculator(){
	}
	
	//OPERATIONS
	@Override
	/**
	 * Polinomul dat ca si parametrul este parcurs, iar intr-o variabila se insumeaza valoare din fiecare monom, de fiecare grad de la 0
	 * la gradul polinomului.
	 */
	public Double valueOfPolynomialFunc(Polynomial polynom,Integer x){
		Double value=0.0;
		Collections.sort(polynom.getMonomList());
		for(int i=0;i<=polynom.getDegree();i++){
			value+= Math.pow(x, polynom.getMonomList().get(i).getDegree())* polynom.getMonomList().get(i).getCoeff().doubleValue();
		}
		System.out.println(value);
		return value;
	}
	@Override
	/**
	 * Determin care este gradul minim intre polinoamele date ca si parametru, parcurg de la 0 la acest grad si insumez coeficientii
	 * celor doua polinoame. La sfarsit se adauga in polinomul rezultat monoamele polinomului de grad mai mare.
	 */
	public Polynomial addPolynomials(Polynomial polynom1, Polynomial polynom2) {
		Polynomial resultPolynom;
		Polynomial maxPolynom;
		ArrayList<Monomial> monomList= new ArrayList<Monomial>();
		Integer minDegree,maxDegree;
		Collections.sort(polynom1.getMonomList());
		Collections.sort(polynom2.getMonomList());
		minDegree = (polynom1.getDegree()<polynom2.getDegree()) ? polynom1.getDegree(): polynom2.getDegree();
		maxPolynom = (polynom1.getDegree()>=polynom2.getDegree()) ? polynom1: polynom2;
		maxDegree=maxPolynom.getDegree();
		for(int i=0;i<=minDegree;i++){
			Monomial resultMonom = new MonomInt((int)polynom1.getMonomList().get(i).getCoeff()+(int) polynom2.getMonomList().get(i).getCoeff(),i);	
			monomList.add(resultMonom);
		}
		for(int i=++minDegree;i<=maxDegree;i++){
			Monomial resultMonom=new MonomInt((int)maxPolynom.getMonomList().get(i).getCoeff(),i);
			monomList.add(resultMonom);
		}
		resultPolynom= new Polynomial(monomList);
		return resultPolynom;
	}

	@Override
	/**
	 * Determin care este gradul minim intre polinoamele date ca si parametru, parcurg de la 0 la acest grad si scad coeficientii
	 * celor doua polinoame. La sfarsit se adauga in polinomul rezultat monoamele polinomului de grad mai mare, cu semnul schimbat
	 * daca acest polinom e al doilea argument.
	 */
	public Polynomial subPolynomials(Polynomial polynom1, Polynomial polynom2) {
		Polynomial resultPolynom;
		Polynomial maxPolynom;
		ArrayList<Monomial> monomList= new ArrayList<Monomial>();
		Integer minDegree,maxDegree;
		Collections.sort(polynom1.getMonomList());
		Collections.sort(polynom2.getMonomList());
		minDegree = (polynom1.getDegree()<polynom2.getDegree()) ? polynom1.getDegree(): polynom2.getDegree();
		maxPolynom = (polynom1.getDegree()>=polynom2.getDegree()) ? polynom1: polynom2;
		maxDegree=maxPolynom.getDegree();
		for(int i=0;i<=minDegree;i++){
			Monomial resultMonom = new MonomInt((int)polynom1.getMonomList().get(i).getCoeff()-(int) polynom2.getMonomList().get(i).getCoeff(),i);	
			monomList.add(resultMonom);
		}

		for(int i=++minDegree;i<=maxDegree;i++){
			Monomial resultMonom;
			if (maxPolynom==polynom2){
				resultMonom=new MonomInt(-(int)maxPolynom.getMonomList().get(i).getCoeff(),i);
			}
			else{
				resultMonom=new MonomInt((int)maxPolynom.getMonomList().get(i).getCoeff(),i);
			}
			monomList.add(resultMonom);
		}
		
		resultPolynom= new Polynomial(monomList);
		return resultPolynom;
	}

	@Override
	/**
	 * Metoda calculeaza coeficientii intr-un vector de intregi, in cadrul a doua bucle for, iar mai apoi intr-o alta bucla
	 * construieste obiecte de tip monom avand acesti coeficienti, obiecte ce vor fi puse intr-o lista de monoame data ca si parametru
	 * in constructorul polinomului rezultat.
	 */
	public Polynomial mulPolynomials(Polynomial polynom1, Polynomial polynom2) {
		Polynomial resultPolynom;
		ArrayList<Monomial> monomList= new ArrayList<Monomial>();
		int[] coeff= new int[polynom1.getDegree()+polynom2.getDegree()+1];
		Collections.sort(polynom1.getMonomList());
		Collections.sort(polynom2.getMonomList());
		for (int i=polynom1.getDegree();i>=0;i--){
			for (int j=polynom2.getDegree();j>=0;j--){
				coeff[i+j]+=(int)polynom1.getMonomList().get(i).getCoeff()*(int)polynom2.getMonomList().get(j).getCoeff();
			}
		}
		for(int i=0;i<=polynom1.getDegree()+polynom2.getDegree();i++){
			Monomial resultMonom= new MonomInt(coeff[i],i);
			monomList.add(resultMonom);
		}
		resultPolynom = new Polynomial(monomList);
		return resultPolynom;
	}

	@Override
	/**
	 * Metoda realizeaza  impartirea a doua polinoame dupa un algoritm descris de Donald Knuth. Metoda returneaza catul, pe cand
	 * restul se va afla in primul parametru.
	 */
	public Polynomial divPolynomials(Polynomial polynom1, Polynomial polynom2) {
		int m,n;
		Polynomial resultPolynom;
		ArrayList<Monomial> monomList= new ArrayList<Monomial>();
		Collections.sort(polynom1.getMonomList());
		Collections.sort(polynom2.getMonomList());
		m=polynom1.getDegree();
		n=polynom2.getDegree();
		if((n==0)&&((int)polynom2.getMonomList().get(0).getCoeff()==0)) throw new ArithmeticException();
		for(int i=(m-n); i>=0;i--){
			Monomial monomResult=new MonomInt((polynom1.getMonomList().get(n+i).getCoeff().intValue())/polynom2.getMonomList().get(n).getCoeff().intValue(),
												 i);
			System.out.println(i);
			monomList.add(monomResult);
		for(int j=(n+i-1);j>=i;j--){
			System.out.println(j);
			polynom1.getMonomList().get(j).setCoeff(polynom1.getMonomList().get(j).getCoeff().doubleValue()-
					monomResult.getCoeff().doubleValue()*polynom2.getMonomList().get(j-i).getCoeff().doubleValue());
		}	
		}
		
		polynom1.setDegree(n-1);
		
		resultPolynom=new Polynomial(monomList);
		Collections.sort(resultPolynom.getMonomList());
		return resultPolynom;
	}

	@Override
	/**
	 * Metoda realizeaza derivarea polinomului dat ca si parametru. Intr-o bucla for, se construiesc monoame
	 * avand coeficientul egal cu gradul monomului din polinomul transmis ca parametru,iar gradul mai mic cu o unitate
	 */
	public Polynomial diffPolynomials(Polynomial polynom) {
		Polynomial resultPolynom;
		ArrayList<Monomial> monomResult= new ArrayList<Monomial>();
		Collections.sort(polynom.getMonomList());
		
		if (polynom.getDegree()==0){
			Monomial resultMonom=new MonomInt(0,0);
			monomResult.add(resultMonom);
		}
		else{
		for(int i=1;i<=polynom.getDegree();i++){
			Monomial resultMonom = new MonomInt( (int)polynom.getMonomList().get(i).getCoeff() * polynom.getMonomList().get(i).getDegree(),
												  (i-1));
			monomResult.add(resultMonom);
			
		}
		}
		resultPolynom= new Polynomial(monomResult);
		return resultPolynom;
		
	}

	@Override
	/**
	 * Metoda calculeaza integrala nedefinita si face diferenta mai apoi intre valorile sale in aboveLimit si
	 * belowLimit. Pentru calculul inegralei nedefinite si al valorilor functiei polinomiale se folosesc metode
	 * descrise in aceasta clasa.
	 */
	public Double defIntPolynomials(Polynomial polynom,Integer belowLimit,Integer aboveLimit) {
		Polynomial resultPolynom= new Polynomial();
		resultPolynom=this.indefIntPolynomials(polynom);
		return this.valueOfPolynomialFunc(resultPolynom, aboveLimit)-this.valueOfPolynomialFunc(resultPolynom, belowLimit);
	}

	@Override
	/**
	 * Metoda calculeaza integrala nedefinita. Intr-o bucla for sunt construite monoamele din polinomul rezultat, 
	 * avand ca si coeficient, coeficientul monomului din polinomul transmis impartit la gradul sau plus o unitate.
	 * Polinomul rezultat va contine monoame cu coeficienti reali (MonomDouble)
	 */
	public Polynomial indefIntPolynomials(Polynomial polynom) {
		Polynomial resultPolynom= new Polynomial();
		ArrayList<Monomial> monomResult= new ArrayList<Monomial>();
		Collections.sort(polynom.getMonomList());
		for(int i=0;i<=polynom.getDegree();i++){
			Monomial monom=new MonomDouble( (polynom.getMonomList().get(i).getCoeff().doubleValue())/
										(polynom.getMonomList().get(i).getDegree()+1),
										polynom.getMonomList().get(i).getDegree()+1);
			monomResult.add(monom);
		}
		monomResult.add(new MonomDouble(0.0,0));
		resultPolynom= new Polynomial(monomResult);
		return resultPolynom;
	}	
	
}
