package model;

/**
 * Clasa abstracta Monomial modeleaza componenta fundamentala a unui polinom. Ea este abstracta intr-un cat nu poate exista
 * de sine statatoare, ci va fi extinsa de clasele MonomInt si MonomDouble care vor specifica natura coeficientului.
 * Metodele neimplementate sunt legatede acest coeficient si se ocupa cu setarea si determinarea lui.
 * @author Bogdan
 *
 */
public abstract class Monomial implements Comparable<Monomial> {
	private Integer degree;

	// CONsTRUCTORs
	public Monomial() {

	}

	public Monomial(Integer degree) {

		this.degree = degree;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * Metoda verifica daca doua monoame au acelasi grad si returneaza o valoare booleana in functie de acest lucru.
	 */
	@Override
	public boolean equals(Object obj){
		if(this.degree== ((Monomial)obj).degree) return true;
		else return false;
		
	}

	// GETTERs & sETTERs
	
	public Integer getDegree() {
		return this.degree;
	}

	public void setDegree(Integer degree) {
		this.degree = degree;
	}
	
	 /**
	  * Metoda returneaza coeficientul monomului. Acesta are tipul Number, urmand sa se faca cast in metodele ce vor implementa efectiv
	  * aceasta metoda, adica cele care vor extinde clasa Monomial.
	  * @return Coeficientul de tip Number
	  */
	public abstract Number getCoeff();
	
	/**
	 * Metoda seteaza coeficientul monomului. Acesta are tipul Number, urmand sa se faca ast in metodele ce vor implementa
	 * aceasta metoda abstracta, adica cele care vor extinde clasa Monomial.
	 * @param coeff
	 */
	public abstract void setCoeff(Number coeff);

	/**
	 * Metoda compareTo primeste suprascrie metoda cu acelasi nume si primind ca parametru un alt monom, il compara cu monomul curent
	 * returnand o valoare intreaga in functie de comparatie.
	 */
	@Override
	 
	public int compareTo(Monomial monom) {
		if (this.degree > monom.degree)
			return 1;
		else if (this.degree == monom.degree)
			return 0;
		else
			return -1;
	}
}
