package model;

public class MonomDouble extends Monomial{
	private Double coeff;
	
	//CONSTRUCTORS
	public MonomDouble(Double coeff, Integer degree){
		super(degree);
		this.coeff=coeff;
	}
	
	//GETTERS AND SETTERS
		public Double getCoeff(){
			return this.coeff;
		}
		public void setCoeff(Number coeff){
			this.coeff= (double) coeff;
		}
	
	
}
