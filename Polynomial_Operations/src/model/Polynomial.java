package model;

import java.util.ArrayList;
import java.util.List;



/**
 * @author Bogdan
 *	
 *	Clasa polinom este cea care modeleaza polinomul, atat operanzii , cat si rezultatele.
 *	Ea contine ca si membrii lista de monoame si gradul polinomului (maximul dintre gradele monoamelor din componenta)
 *
 */

public class Polynomial {
	
	private List<Monomial> monomList;
	private Integer degree;
	
	//CONSTRUCTORS
	
	public Polynomial (){
	monomList = new ArrayList<Monomial>();
	}
	public Polynomial(List<Monomial> monomList){
		this.monomList=monomList;
		this.degree=this.detDegree(monomList);
	}
	
	// GETTERS && SETTERS
	
	/**
	 * Metoda returneaza lista de monoame a polinomului, modelata printr-un ArrayList
	 * @return Lista de monoame a polinomului
	 */
	public List<Monomial> getMonomList(){
		return this.monomList;
	}
	
	/**
	 * Metoda returneaza gradul polinomului.
	 * @return gradul polinomului.
	 */
	public Integer getDegree(){
		return this.degree;
	}
	
	/**
	 * Metoda seteaza lista de monoame a polinomului.
	 * @param monomList Noua lista de monoame.
	 */
	public void setMonomList(List<Monomial> monomList){
		this.monomList=monomList;
	}
	
	/**
	 * Metoda seteaza gradul polinomului in cazul in care acesta a fost schimbat.
	 * @param degree Noul grad al polinomului
	 */
	public void setDegree(Integer degree){
		this.degree=degree;
	}
	
	//DETERMINE THE DEGREE
	
	/**
	 * Metoda determina gradul polinomului. Ea este apleata in constructor
	 * @param monomList
	 * @return Returneaza cel mai mare grad din lista de monoame data ca parametru, cu alte cuvinte, gradul polinomului.
	 * 		   Acesta va fi stocat intr-un membru, fiind necesar mai apoi pentru operatii.
	 */
	public Integer detDegree(List<Monomial> monomList){
		Integer max=0;		
		for(Monomial mon:monomList)
			if (max<(double)mon.getDegree() && mon.getCoeff()!= (Number)0) max=mon.getDegree();
		return max;
	}
	
	/**
	 * 
	 * Metoda verifica daca exista in cadrul polinomului un monom de un anumit grad. Aparent inutila, metoda este insa de mare
	 * ajutor mai ales la procesarea datelor de intrare, cand daca nu exista un anumit monom de un grad mai mic decat gradul
	 * polinomului, acesta trebuie creat si adaugat in polinom avand, evident, coeficientul zero. Lucrul acesta este necesar
	 * intrucat utilizatorul nu va introduce si coeficientii zero cu monoamele corespunzatoare.
	 * 
	 * @param x Gradul pentru care se verifica
	 * @return Returneaza o valoare booleana dupa cum a fost sau nu gasit monomul de grad x.
	 */
	
	public boolean existsMonom(Integer x){
		for(Monomial monom:this.monomList){
			if (monom.getDegree()==x) return true;
		}
		return false;
	}

}
