package model;

/**
 * Interafata este implementata de clasa Calculator si specifica principalele operatii pe care le poate
 *  efectua aceata prin metodele sale.
 * @author Bogdam
 *
 */

public interface Operations {
	
	/**
	 * 
	 * @param polynom Functia polinomiala.	
	 * @param x Punctul in care se va calcula valoarea functiei polinomiale.
	 * @return Returneaza un numar real reprezentand valoarea functiei in punctul x.
	 */
	public Double valueOfPolynomialFunc(Polynomial polynom,Integer x);
	/**
	 * Metoda realizeaza operatie de adunare a polinoamelor date ca si parametru
	 * @param polynom1 operand1
	 * @param polynom2 operand2
	 * @return Rezutatul operatiei de adunare
	 */
	public Polynomial addPolynomials(Polynomial polynom1, Polynomial polynom2);
	/**
	 * Metoda realizeaza diferenta polinoamelor date ca si parametru. Astfel, primul parametru e descazutul, al doilea e
	 * scazatorul iar valoarea returnata diferenta
	 * @param polynom1	Descazut
	 * @param polynom2	Scazator
	 * @return Rezultatul scaderii
	 */
	public Polynomial subPolynomials(Polynomial polynom1, Polynomial polynom2);
	/**
	 * Metoda realizeaza inmultirea polinoamelor date ca si parametru si returneaza rezultatul acestei operatii
	 * @param polynom1 Operand 1
	 * @param polynom2 Operand 2
	 * @return Rezultatul inmultirii.
	 */
	public Polynomial mulPolynomials(Polynomial polynom1, Polynomial polynom2);
	/**
	 * Metoda realizeaza impartirea polinoamelor date ca si parametru si returneaza catul.
	 * @param polynom1 Initial deimpartitul. La finalul metodei acesta va fi restul
	 * @param polynom2 Impartiroul
	 * @return Catul, adica rezultatul impartirii
	 */
	public Polynomial divPolynomials(Polynomial polynom1, Polynomial polynom2);
	/**
	 * Aceasta metoda realizeaza oepratia de derivare asupra polinomului transmis ca si parametru.
	 * @param polynom Polinomul care va fi derivat.
	 * @return Rezultatul operatiei de derivare: tot un polinom
	 */
	public Polynomial diffPolynomials(Polynomial polynom);
	/**
	 * Aceasta metoda realizeaza operatia de integrare definita. Primeste ca si parametrii pe langa polinomul de integrat, 
	 * limitele de integrare.
	 * @param polynom Polinomul de integrat
	 * @param belowLimit Limita inferioara a integrarii
	 * @param aboveLimit Limita superioara a integrarii
	 * @return Returneaza o valoare reala, reprezentand rezultatul final al operatiei de integrare definita.
	 */
	public Double defIntPolynomials(Polynomial polynom,Integer belowLimit,Integer aboveLimit); 
	/**
	 * Metoda realizeaza operatia de integrare nedefinita. Primeste ca si parametu polinomul de integrat.
	 * @param polynom Polinomul de integrat
	 * @return Rezultatul operatiei de integrare.
	 */
	public Polynomial indefIntPolynomials(Polynomial polynom);
	
	

}
