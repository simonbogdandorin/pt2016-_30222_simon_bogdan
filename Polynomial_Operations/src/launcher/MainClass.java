package launcher;


import javax.swing.SwingUtilities;

import controller.*;
import view.*;
import model.*;


public class MainClass {

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {                                           
            	TheView theView= new TheView();
            	TheModel theModel = new TheModel();
        		TheController theController = new TheController(theView,theModel);
        		theController.control();
            }
        });  
       
	}
	

}
		