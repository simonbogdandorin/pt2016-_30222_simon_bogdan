package launcher;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Client;
import model.Processor;
import model.Queue;

public class JUnitTest {

	@Test
	public void test() {
		
		Client client = new Client(1,3,5);
		Client client2= new Client(2,6,7);
		Queue queue = new Queue();
		queue.getClients().add(client);
		queue.getClients().add(client2);
		
		Thread thread = new Thread(queue);
		thread.start();
		try {
			Thread.sleep(13000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int x=queue.getQueueTotalServiceTime();
		assertEquals(x,12);
		x=queue.getQueueTotalWaitingTime();
		assertEquals(x,3);
		
	}

}
