package launcher;

import javax.swing.SwingUtilities;

import controller.TheController;
import model.Processor;
import view.TheView;

public class MainClass {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {                                           
            	TheView theView= new TheView();
            	Processor processor;
        		TheController theController = new TheController(theView);
        		theController.control();
            }
        });  
       
	}
	

}


