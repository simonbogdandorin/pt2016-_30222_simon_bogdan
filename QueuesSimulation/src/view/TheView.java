package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;



import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.TextArea;

public class TheView {

	private JFrame frmQueuessimulator;
	private CardLayout cardLayout;
	
	//START PANEL
	private JTextField minArrTimeTextField;
	private JTextField maxArrTimeTextField;
	private JTextField minServiceTimeTextField;
	private JTextField maxServiceTimeTextField;
	private JTextField numberOfQueuesTextField;
	private JTextField simulationTimeTextField;
	private JButton btnStart;
	
	
	
	//MAIN PANEL
	private JTable[] queuesTables;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TheView window = new TheView();
					window.frmQueuessimulator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TheView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Object[] columnNames= new Object[]{"C: ","TS: "
		};
		
		queuesTables=new JTable[5];
		//FRAME
		frmQueuessimulator = new JFrame();
		frmQueuessimulator.setVisible(true);
		frmQueuessimulator.setTitle("QueuesSimulator\r\n");
		frmQueuessimulator.setResizable(false);
		frmQueuessimulator.setBounds(100, 100, 1029, 543);
		frmQueuessimulator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cardLayout= new CardLayout(0,0);
		frmQueuessimulator.getContentPane().setLayout(cardLayout);
		
		/* **** START PANEL **** */
		
		//PANEL
		JPanel startPanel = new JPanel();
		frmQueuessimulator.getContentPane().add(startPanel, "startPanel");
		//START BTN
		btnStart = new JButton("START");
		btnStart.setForeground(new Color(0, 0, 102));
		btnStart.setFont(new Font("Bodoni MT", Font.BOLD, 25));
		//LABELS
		JLabel lblIntervalulDeGenerare = new JLabel("Intervalul de generare a clientilor:");
		JLabel lblIntervalulTimpilorDeServire = new JLabel("Intervalul timpilor de servire:");
		JLabel lblNumarulCozilor = new JLabel("Numarul cozilor:");
		JLabel lblTimpulDeSimulare = new JLabel("Timpul de simulare:");
		//TEXT FIELDS
		minArrTimeTextField = new JTextField();
		minArrTimeTextField.setColumns(10);
		maxArrTimeTextField = new JTextField();
		maxArrTimeTextField.setColumns(10);
		minServiceTimeTextField = new JTextField();
		minServiceTimeTextField.setColumns(10);
		maxServiceTimeTextField = new JTextField();
		maxServiceTimeTextField.setColumns(10);
		numberOfQueuesTextField = new JTextField();
		numberOfQueuesTextField.setColumns(10);
		simulationTimeTextField = new JTextField();
		simulationTimeTextField.setColumns(10);
		//GROUP LAYOUT
		GroupLayout gl_startPanel = new GroupLayout(startPanel);
		gl_startPanel.setHorizontalGroup(
			gl_startPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_startPanel.createSequentialGroup()
					.addGap(165)
					.addGroup(gl_startPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_startPanel.createSequentialGroup()
							.addGroup(gl_startPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_startPanel.createSequentialGroup()
									.addGap(91)
									.addComponent(lblNumarulCozilor, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_startPanel.createSequentialGroup()
									.addGap(84)
									.addComponent(lblTimpulDeSimulare, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)))
							.addGap(4)
							.addGroup(gl_startPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_startPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnStart, GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE))
								.addGroup(Alignment.TRAILING, gl_startPanel.createSequentialGroup()
									.addGap(124)
									.addGroup(gl_startPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(simulationTimeTextField, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
										.addComponent(numberOfQueuesTextField, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
									.addGap(120))))
						.addGroup(gl_startPanel.createSequentialGroup()
							.addGap(8)
							.addGroup(gl_startPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_startPanel.createSequentialGroup()
									.addGap(24)
									.addComponent(lblIntervalulTimpilorDeServire, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(minServiceTimeTextField, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 176, Short.MAX_VALUE)
									.addComponent(maxServiceTimeTextField, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_startPanel.createSequentialGroup()
									.addComponent(lblIntervalulDeGenerare, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(minArrTimeTextField, GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
									.addGap(176)
									.addComponent(maxArrTimeTextField, GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)))))
					.addGap(341))
		);
		gl_startPanel.setVerticalGroup(
			gl_startPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_startPanel.createSequentialGroup()
					.addGap(101)
					.addGroup(gl_startPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(minArrTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(maxArrTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblIntervalulDeGenerare, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_startPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(minServiceTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(maxServiceTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblIntervalulTimpilorDeServire, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_startPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(numberOfQueuesTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNumarulCozilor, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
					.addGroup(gl_startPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTimpulDeSimulare, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(simulationTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(47)
					.addComponent(btnStart, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(150))
		);
		startPanel.setLayout(gl_startPanel);
		
		/* ***** MAIN PANEL ***** */
		JPanel mainPanel = new JPanel();
		frmQueuessimulator.getContentPane().add(mainPanel, "mainPanel");
		
		//TABLES
		for(int i=0;i<5;i++){
			queuesTables[i]=new JTable(new DefaultTableModel());
		}
		queuesTables[0].setBounds(65, 35, 107, 288);
		queuesTables[1].setBounds(263, 35, 107, 288);
		queuesTables[2].setBounds(460, 35, 107, 288);
		queuesTables[3].setBounds(657, 35, 107, 288);
		queuesTables[4].setBounds(849, 35, 107, 288);
		mainPanel.setLayout(null);
		mainPanel.add(queuesTables[0]);
		mainPanel.add(queuesTables[1]);
		mainPanel.add(queuesTables[2]);
		mainPanel.add(queuesTables[3]);
		mainPanel.add(queuesTables[4]);
		
		//LABELS
		JLabel lblCoada = new JLabel("COADA 1");
		lblCoada.setBounds(90, 11, 80, 14);
		mainPanel.add(lblCoada);
		JLabel lblCoada_1 = new JLabel("COADA 2");
		lblCoada_1.setBounds(290, 11, 68, 14);
		mainPanel.add(lblCoada_1);
		JLabel lblCoada_2 = new JLabel("COADA 3");
		lblCoada_2.setBounds(490, 11, 77, 14);
		mainPanel.add(lblCoada_2);
		JLabel lblCoada_3 = new JLabel("COADA 4");
		lblCoada_3.setBounds(691, 11, 73, 14);
		mainPanel.add(lblCoada_3);
		JLabel lblCoada_4 = new JLabel("COADA 5");
		lblCoada_4.setBounds(879, 11, 77, 14);
		mainPanel.add(lblCoada_4);
		
		JLabel lblNr = new JLabel("Nr.");
		lblNr.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblNr.setBounds(85, 23, 15, 14);
		mainPanel.add(lblNr);
		
		JLabel lblTmpDeServ = new JLabel("T de Serv.");
		lblTmpDeServ.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblTmpDeServ.setBounds(122, 23, 58, 14);
		mainPanel.add(lblTmpDeServ);
		
		JLabel label = new JLabel("Nr.");
		label.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label.setBounds(283, 23, 15, 14);
		mainPanel.add(label);
		
		JLabel label_1 = new JLabel("T de Serv.");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_1.setBounds(319, 23, 58, 14);
		mainPanel.add(label_1);
		
		JLabel label_2 = new JLabel("Nr.");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_2.setBounds(483, 23, 15, 14);
		mainPanel.add(label_2);
		
		JLabel label_3 = new JLabel("T de Serv.");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_3.setBounds(518, 23, 58, 14);
		mainPanel.add(label_3);
		
		JLabel label_4 = new JLabel("Nr.");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_4.setBounds(680, 23, 15, 14);
		mainPanel.add(label_4);
		
		JLabel label_5 = new JLabel("T de Serv.");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_5.setBounds(716, 23, 58, 14);
		mainPanel.add(label_5);
		
		JLabel label_6 = new JLabel("Nr.");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_6.setBounds(871, 23, 15, 14);
		mainPanel.add(label_6);
		
		JLabel label_7 = new JLabel("T de Serv.");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label_7.setBounds(906, 23, 58, 14);
		mainPanel.add(label_7);
		
		
		textArea = new JTextArea();
		textArea.setBounds(85, 328, 843, 160);
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(85, 328, 843, 160);
		mainPanel.add(scrollPane);
		
	}
	
	public void displayMessageDialog(String message){
		JOptionPane.showMessageDialog(frmQueuessimulator, message);
	}

	//GETTERS & SETTERS
	
	public JFrame getFrmQueuessimulator() {
		return frmQueuessimulator;
	}

	public void setFrmQueuessimulator(JFrame frmQueuessimulator) {
		this.frmQueuessimulator = frmQueuessimulator;
	}

	public JTextField getMinArrTimeTextField() {
		return minArrTimeTextField;
	}

	public void setMinArrTimeTextField(JTextField minArrTimeTextField) {
		this.minArrTimeTextField = minArrTimeTextField;
	}

	public JTextField getMaxArrTimeTextField() {
		return maxArrTimeTextField;
	}

	public void setMaxArrTimeTextField(JTextField maxArrTimeTextField) {
		this.maxArrTimeTextField = maxArrTimeTextField;
	}

	public JTextField getMinServiceTimeTextField() {
		return minServiceTimeTextField;
	}

	public void setMinServiceTimeTextField(JTextField minServiceTimeTextField) {
		this.minServiceTimeTextField = minServiceTimeTextField;
	}

	public JTextField getMaxServiceTimeTextField() {
		return maxServiceTimeTextField;
	}

	public void setMaxServiceTimeTextField(JTextField maxServiceTimeTextField) {
		this.maxServiceTimeTextField = maxServiceTimeTextField;
	}

	public JTextField getNumberOfQueuesTextField() {
		return numberOfQueuesTextField;
	}

	public void setNumberOfQueuesTextField(JTextField numberOfQueuesTextField) {
		this.numberOfQueuesTextField = numberOfQueuesTextField;
	}

	public JTextField getSimulationTimeTextField() {
		return simulationTimeTextField;
	}

	public void setSimulationTimeTextField(JTextField simulationTimeTextField) {
		this.simulationTimeTextField = simulationTimeTextField;
	}

	public JButton getBtnStart() {
		return btnStart;
	}

	public void setBtnStart(JButton btnStart) {
		this.btnStart = btnStart;
	}
	
	public CardLayout getCardLayout(){
		return cardLayout;
	}
	
	public JTextArea getTextArea(){
		return textArea;
	}
	
	public JTable[] getQueuesTables(){
		return queuesTables;
	}
}
