package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.table.DefaultTableModel;
import javax.swing.text.StyledDocument;

import model.Client;
import model.Processor;

import view.TheView;

public class TheController {
	private ActionListener startBtnActionListener;
	private Processor theProcessor;
	private TheView theView;
	
	
	public TheController(TheView theView){
		this.theView=new TheView();
		
		
		
	}
	
	public boolean verifyInput(){
		try{
		if((Integer.parseInt(theView.getMinArrTimeTextField().getText())<0)||(Integer.parseInt(theView.getMinArrTimeTextField().getText())>=Integer.parseInt(theView.getMaxArrTimeTextField().getText()))
				||(Integer.parseInt(theView.getMinServiceTimeTextField().getText())<0)||(Integer.parseInt(theView.getMinServiceTimeTextField().getText())>=Integer.parseInt(theView.getMaxServiceTimeTextField().getText())
			    ||(Integer.parseInt(theView.getNumberOfQueuesTextField().getText())<=0)||(Integer.parseInt(theView.getSimulationTimeTextField().getText())<=0)))
			return false;
		else
			return true;
		}
		catch(NumberFormatException e){
			return false;
		}
	}
	
	
	public void control(){
		MessageConsole mc = new MessageConsole(theView.getTextArea());
		mc.redirectOut();
		mc.redirectErr(Color.RED, null);
		mc.setMessageLines(100);
		
		
		theView.getCardLayout().show(theView.getFrmQueuessimulator().getContentPane(), "startPanel");
		startBtnActionListener=new ActionListener(){
			public void actionPerformed(ActionEvent event){
				if (verifyInput()){
					theView.getCardLayout().show(theView.getFrmQueuessimulator().getContentPane(), "mainPanel");
					theProcessor=new Processor(Integer.parseInt(theView.getMinArrTimeTextField().getText()),Integer.parseInt(theView.getMaxArrTimeTextField().getText()),
							Integer.parseInt(theView.getNumberOfQueuesTextField().getText()),Integer.parseInt(theView.getSimulationTimeTextField().getText()),
							Integer.parseInt(theView.getMinServiceTimeTextField().getText()),Integer.parseInt(theView.getMaxServiceTimeTextField().getText()));
					Thread mainThread = new Thread(theProcessor);
					mainThread.start();
					initTables();
					Timer timer = new Timer();
			        timer.scheduleAtFixedRate(new TimerTask()
			            {   int counter=0;
			                public void run()
			                {	
			                	if (counter<Integer.parseInt(theView.getSimulationTimeTextField().getText())+1){
			                		counter++;
			                		if(counter<=Integer.parseInt(theView.getSimulationTimeTextField().getText()))
			                		updateTables();
			                	}
			                	else{
			                		timer.cancel();
			                	}
			                }
			                },0,1000);
				}
				else {
					
					theView.displayMessageDialog("Atentie la introducerea datelor!");
				}
				
			}
			};;
			theView.getBtnStart().addActionListener(startBtnActionListener);
	}
	
	public void updateTables(){
		int i;
		for(i=0;i<theProcessor.getNumberOfQueues();i++){
			DefaultTableModel model = (DefaultTableModel) (theView.getQueuesTables())[i].getModel();
			model.setRowCount(0);
			for(Client client: theProcessor.getQueues().get(i).getClients()){
				Object[] data= new Object[2];
				data[0]=client.getId();
				data[1]=client.getServiceTime();
				model.addRow(data);
			}
			theView.getQueuesTables()[i].repaint();
		}
		
		
	}
	
	public void initTables(){
		String[] colNames= new String[]{
			"C","Tmp. Serv"
		};
		for(int i=0;i<theProcessor.getNumberOfQueues();i++){
			DefaultTableModel model = (DefaultTableModel) (theView.getQueuesTables())[i].getModel();
			for(int j=0;j<2;j++){
				model.addColumn(colNames[j]);
			}
		}
	}
}
