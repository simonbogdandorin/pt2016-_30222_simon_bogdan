package model;

public class Client {
	
	private Integer id;
	private Integer arrivalTime;
	private Integer serviceTime;
	
	//CONSTRUCTORS
	public Client(Integer id,Integer arrivalTime, Integer serviceTime){
		this.id=id;
		this.arrivalTime=arrivalTime;
		this.serviceTime=serviceTime;
	}
	
	
	//GETTERS & SETTERS
	public Integer getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Integer arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public Integer getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(Integer serviceTime) {
		this.serviceTime = serviceTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
}
