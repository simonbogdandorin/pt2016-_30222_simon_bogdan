package model;

import java.util.concurrent.LinkedTransferQueue;

public class Queue implements Runnable {
	Integer emptyQueueTime;
	Integer queueTotalWaitingTime;
	Integer queueTotalServiceTime;
	Integer queueNumberOfClients;
	LinkedTransferQueue<Client> clients = new LinkedTransferQueue<Client>();
	
	
	public Queue(){
		this.emptyQueueTime=0;
		this.queueTotalWaitingTime=0;
		this.queueTotalServiceTime=0;
		this.queueNumberOfClients=0;
	}
	public Queue(LinkedTransferQueue<Client> clients){
	
		this.clients=clients;
		//updateWaitingTime();
		this.emptyQueueTime=0;
		this.queueTotalWaitingTime=0;
		this.queueTotalServiceTime=0;
		this.queueNumberOfClients=0;
	}
	
	@Override
	public void run() {
		int counter=0;
		int serviceTime=1;
		while(true){
			Thread thread = Thread.currentThread();
			if (clients.isEmpty())
				emptyQueueTime++;
			else 
				if (serviceTime==clients.peek().getServiceTime()){
					int x=((counter-clients.peek().getArrivalTime())-clients.peek().getServiceTime());
					queueTotalWaitingTime=queueTotalWaitingTime+x;
					//System.out.println("#"+x+"#");
					queueTotalServiceTime= queueTotalServiceTime+clients.peek().getServiceTime();
					queueNumberOfClients++;
					System.out.println("Clientul "+clients.peek().getId()+" a iesit!");
					clients.remove();
					serviceTime=1;
				}
				else{
					serviceTime++;
				}
			try {
				counter++;
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			
				return;
			}
		}
	}
	
	public int getTotalServiceTime(){
		int sum=0;
		for(Client client:clients){
			sum+=client.getServiceTime();
		}
		return sum;
	}
	
	//GETTERS & SETTERS
	public Integer getEmptyQueueTime() {
		return emptyQueueTime;
	}
	public void setEmptyQueueTime(Integer emptyQueueTime) {
		this.emptyQueueTime = emptyQueueTime;
	}
	public LinkedTransferQueue<Client> getClients() {
		return clients;
	}
	public void setClients(LinkedTransferQueue<Client> clients) {
		this.clients = clients;
	}
	
	public Integer getQueueTotalWaitingTime() {
		return queueTotalWaitingTime;
	}
	public void setQueueTotalWaitingTime(Integer queueTotalWaitingTime) {
		this.queueTotalWaitingTime = queueTotalWaitingTime;
	}
	public Integer getQueueTotalServiceTime() {
		return queueTotalServiceTime;
	}
	public void setQueueTotalServiceTime(Integer queueTotalServiceTime) {
		this.queueTotalServiceTime = queueTotalServiceTime;
	}
	public Integer getQueueNumberOfClients() {
		return queueNumberOfClients;
	}
	public void setQueueNumberOfClients(Integer queueNumberOfClients) {
		this.queueNumberOfClients = queueNumberOfClients;
	}
	
}
