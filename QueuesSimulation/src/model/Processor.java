package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Processor implements Runnable {
	private List<Queue> queues= new ArrayList<Queue>();
	private List<Thread> threads= new ArrayList<Thread>();
	//INPUTS
	private Integer minClientsArrTime;
	private Integer maxClientsArrTime;
	private Integer minClientsServiceTime;
	private Integer maxClientsServiceTime;
	private Integer simulationTime;
	private Integer numberOfQueues;
	//CONTROL
	private Integer numberOfClients;
	private Integer totalWaitingTime;
	private Integer totalServiceTime;
	private Integer totalEmptyQueuesTime;
	private Integer peakHour;



	//CONSTRUCTORS
	public Processor(Integer minClientsArrTime,Integer maxClientsArrTime,Integer numberOfQueues, Integer simulationTime,Integer minClientsServiceTime,
					 Integer maxClientsServiceTime){
		this.minClientsArrTime=minClientsArrTime;
		this.maxClientsArrTime=maxClientsArrTime;
		this.minClientsServiceTime=minClientsServiceTime;
		this.maxClientsServiceTime=maxClientsServiceTime;
		this.numberOfQueues=numberOfQueues;
		this.simulationTime=simulationTime;
		this.numberOfClients=0;
		this.totalWaitingTime=0;
		this.totalServiceTime=0;
		this.totalEmptyQueuesTime=0;
		queues= new ArrayList<Queue>();
		createQueues(numberOfQueues);
	}
	
	boolean  createQueues(Integer numberOfQueues){
		Queue queue;
		if(numberOfQueues>0){
			for(int i=1;i<=numberOfQueues;i++){
				queue= new Queue();
				Thread th=new Thread(queue,"Thread "+String.valueOf(i));
				threads.add(th);
				queues.add(queue);
				th.start();
			}
			return true;
		}
		else{
			return false;
		}
	}
	
	public void interruptThreads(Integer numberOfQueues){
			for(int i=0;i<numberOfQueues;i++){
				threads.get(i).interrupt();
				totalWaitingTime= totalWaitingTime + queues.get(i).getQueueTotalWaitingTime();
				totalServiceTime= totalServiceTime + queues.get(i).getQueueTotalServiceTime();
				totalEmptyQueuesTime= totalEmptyQueuesTime + queues.get(i).getEmptyQueueTime();
				//numberOfClients=numberOfClients+queues.get(i).getQueueNumberOfClients();
			}
			return;
	}
	
	public int getNextQueue(){
		
		int minWaitingTime=queues.get(0).getTotalServiceTime();
		int minIndex=0;
		for(int i=1;i<numberOfQueues;i++){
			if(queues.get(i).getTotalServiceTime()<minWaitingTime){
				minWaitingTime=queues.get(i).getTotalServiceTime();
				minIndex=i;
			}
		}
		return minIndex;
	}
	
	public int totalNumberOfClients(){
		int nr=0;
		for(Queue queue:queues){
			nr+=queue.getClients().size();
		}
		return nr;
	}
	
	@Override
	public void run() {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask()
			{	int currTimeBetweenClients=minClientsArrTime + (int)(Math.random() * ((maxClientsArrTime - minClientsArrTime) + 1));;
		    	int secondsCounter=0;
		    	int timeBetweenClientsCounter=1;
		    	int currClientServiceTime;
		    	int maxNumberOfClients=0;
		    	
		        public void run()
		        {
		        if (secondsCounter<simulationTime){
		        	if(maxNumberOfClients<totalNumberOfClients()){
		        		maxNumberOfClients=totalNumberOfClients();
		        		peakHour=secondsCounter;
		        	}
		        	secondsCounter=secondsCounter+1;
		        	System.out.println("Secunda "+secondsCounter);
		            if (timeBetweenClientsCounter==currTimeBetweenClients){
		            	currClientServiceTime=	minClientsServiceTime + (int)(Math.random() * ((maxClientsServiceTime - minClientsServiceTime) + 1));
		            	numberOfClients++;
		            	Client client = new Client(numberOfClients,secondsCounter-1,currClientServiceTime);
		            	int i=getNextQueue();
		            	queues.get(i).clients.add(client);
		            	System.out.println("Clientul "+numberOfClients+" a fost adaugat la coada "+i+". Timp de serviciu: "+currClientServiceTime+",arrival time: "+client.getArrivalTime());
			        	currTimeBetweenClients=	minClientsArrTime + (int)(Math.random() * ((maxClientsArrTime - minClientsArrTime) + 1));
			        	timeBetweenClientsCounter=1; 	
			        }
			        else {
			        	timeBetweenClientsCounter=timeBetweenClientsCounter +1;
			        }
		        }
		        else{
		        	interruptThreads(numberOfQueues);
		        	
		        	System.out.println("####### //////// #######");
					System.out.println("Timpul mediu de asteptare la coada: "+(double)totalWaitingTime/numberOfClients+".");
					System.out.println("Timpul mediu de serviciu: "+(double)totalServiceTime/numberOfClients+".");
					System.out.println("Timpul mediu de cozi goale: "+(double)totalEmptyQueuesTime/numberOfQueues+".");
					System.out.println("Ora de varf: "+peakHour+".");
					System.out.println("####### //////// #######");
		        	timer.cancel();
		        	
		        }
		            }
		        }, 0, 1000);
    }


	//GETTERS & SETTERS
	public List<Queue> getQueues() {
		return queues;
	}

	public void setQueues(List<Queue> queues) {
		this.queues = queues;
	}

	public List<Thread> getThreads() {
		return threads;
	}

	public void setThreads(List<Thread> threads) {
		this.threads = threads;
	}

	public Integer getMinClientsArrTime() {
		return minClientsArrTime;
	}

	public void setMinClientsArrTime(Integer minClientsArrTime) {
		this.minClientsArrTime = minClientsArrTime;
	}

	public Integer getMaxClientsArrTime() {
		return maxClientsArrTime;
	}

	public void setMaxClientsArrTime(Integer maxClientsArrTime) {
		this.maxClientsArrTime = maxClientsArrTime;
	}

	public Integer getMinClientsServiceTime() {
		return minClientsServiceTime;
	}

	public void setMinClientsServiceTime(Integer minClientsServiceTime) {
		this.minClientsServiceTime = minClientsServiceTime;
	}

	public Integer getMaxClientsServiceTime() {
		return maxClientsServiceTime;
	}

	public void setMaxClientsServiceTime(Integer maxClientsServiceTime) {
		this.maxClientsServiceTime = maxClientsServiceTime;
	}

	public Integer getSimulationTime() {
		return simulationTime;
	}

	public void setSimulationTime(Integer simulationTime) {
		this.simulationTime = simulationTime;
	}

	public Integer getNumberOfQueues() {
		return numberOfQueues;
	}

	public void setNumberOfQueues(Integer numberOfQueues) {
		this.numberOfQueues = numberOfQueues;
	}

	public Integer getNumberOfClients() {
		return numberOfClients;
	}

	public void setNumberOfClients(Integer numberOfClients) {
		this.numberOfClients = numberOfClients;
	}

	public Integer getTotalWaitingTime() {
		return totalWaitingTime;
	}

	public void setTotalWaitingTime(Integer totalWaitingTime) {
		this.totalWaitingTime = totalWaitingTime;
	}

	public Integer getTotalServiceTime() {
		return totalServiceTime;
	}

	public void setTotalServiceTime(Integer totalServiceTime) {
		this.totalServiceTime = totalServiceTime;
	}

	public Integer getTotalEmptyQueuesTime() {
		return totalEmptyQueuesTime;
	}

	public void setTotalEmptyQueuesTime(Integer totalEmptyQueuesTime) {
		this.totalEmptyQueuesTime = totalEmptyQueuesTime;
	}

	public Integer getPeakHour() {
		return peakHour;
	}

	public void setPeakHour(Integer peakHour) {
		this.peakHour = peakHour;
	}	
}
